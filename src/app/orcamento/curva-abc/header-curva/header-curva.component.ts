import { Component, Input, OnInit } from '@angular/core'
import { CurvaAbcGrupo } from '../../../models'
import { FadeOutAnimation } from '../../../shared/animations/fadeOut'

@Component({
  selector: 'app-header-curva',
  templateUrl: './header-curva.component.html',
  styleUrls: ['./header-curva.component.scss'],
  animations: [FadeOutAnimation],
})
export class HeaderCurvaComponent implements OnInit {
  @Input() consistencyInfo: {
    meta: number
    especialistas: number
    fornecedores: number
  } = {
    meta: 0,
    especialistas: 0,
    fornecedores: 0,
  }

  @Input() totalGoalValue: number
  @Input() totalEstimatedValue: number
  @Input() grupos: CurvaAbcGrupo[]

  constructor() {}

  ngOnInit() {}
}
