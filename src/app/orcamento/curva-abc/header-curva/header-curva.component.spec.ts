import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { HeaderCurvaComponent } from './header-curva.component'

describe('HeaderCurvaComponent', () => {
  let component: HeaderCurvaComponent
  let fixture: ComponentFixture<HeaderCurvaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderCurvaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderCurvaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
