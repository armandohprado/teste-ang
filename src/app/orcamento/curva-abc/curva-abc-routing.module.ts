import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CurvaABCComponent } from './curva-abc.component'

const routes: Routes = [
  {
    path: '',
    component: CurvaABCComponent,
    data: {
      breadcrumb: 'Teste'
    }
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurvaABCRoutingModule {}