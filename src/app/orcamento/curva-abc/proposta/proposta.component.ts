import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core'
import {
  Grupo,
  Orcamento,
  Proposta,
  StatusProposta,
  StatusPropostaValorOrcamento,
} from '../../../models'
import { FormBuilder, FormGroup } from '@angular/forms'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { BehaviorSubject } from 'rxjs'
import moment from 'moment'
import { TemplateVariableDirective } from '../../../shared/directives/template-variable.directive'

@Component({
  selector: 'app-proposta',
  templateUrl: './proposta.component.html',
  styleUrls: ['./proposta.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropostaComponent implements OnInit {
  @Input()
  grupo: Grupo

  @ViewChild('isActive', { static: true }) isActive: TemplateVariableDirective

  orcamento$: BehaviorSubject<Orcamento> = this.orcamentoService.orcamento$
  formComments: FormGroup
  formComplementGroup: FormGroup

  collapse = true
  toggleFornecedoresStatus: boolean

  statusProposta: typeof StatusProposta = StatusProposta
  statusPropostaValorOrcamento: typeof StatusPropostaValorOrcamento = StatusPropostaValorOrcamento

  constructor(
    private formBuilder: FormBuilder,
    private orcamentoService: OrcamentoService
  ) {}

  get propostas(): Proposta[] {
    if (this.grupo) {
      const toAppearFirst = []
      const others = []
      this.grupo.propostas.forEach(proposta => {
        const fornecedor = this.getFornecedor(proposta.idFornecedor)
        if (
          fornecedor.nomeFantasia.toLowerCase().includes('especialista') ||
          fornecedor.nomeFantasia.toLowerCase().includes('estimado')
        ) {
          toAppearFirst.push(proposta)
        } else {
          others.push(proposta)
        }
      })
      toAppearFirst.sort()
      others.sort()
      return toAppearFirst.concat(others)
    }
    return []
  }

  ngOnInit() {
    this.setUpForms()
  }

  submit(evt: KeyboardEvent) {
    if (!evt.shiftKey) {
      evt.preventDefault()
    }
  }

  checkDate(date) {
    return moment().isAfter(date)
  }

  getFornecedor(idFornecedor: number) {
    return this.grupo.fornecedores.find(
      fornecedor => fornecedor.idFornecedor === idFornecedor
    )
  }

  submitComplementGroupForm() {
    this.isActive.templateVar = !this.isActive.templateVar
  }

  private setUpForms() {
    this.formComments = this.formBuilder.group({
      comentarioGrupo: this.formBuilder.control(this.grupo.comentarioGrupo),
      exibeComentarioPropostaGrupo: this.formBuilder.control(
        this.grupo.exibeComentarioPropostaGrupo
      ),
    })

    this.formComplementGroup = this.formBuilder.group({
      complementoGrupo: this.formBuilder.control(this.grupo.complementoGrupo),
    })
  }
}
