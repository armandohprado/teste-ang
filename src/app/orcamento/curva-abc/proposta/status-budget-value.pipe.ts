import { Pipe, PipeTransform } from '@angular/core'
import { Proposta, StatusPropostaValorOrcamento } from '../../../models'

@Pipe({
  name: 'statusBudgetValue',
})
export class StatusBudgetValuePipe implements PipeTransform {
  transform(proposta: Proposta): string {
    if (proposta.desatualizadoProposta) {
      return StatusPropostaValorOrcamento.Desatualizado
    } else if (proposta.valorParcialProposta && proposta.valorTotalProposta) {
      return StatusPropostaValorOrcamento.Parcial
    }
    return StatusPropostaValorOrcamento.Total
  }
}
