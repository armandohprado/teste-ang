import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core'
import { OrcamentoService } from '../../services/orcamento/orcamento.service'
import { BehaviorSubject, combineLatest, Observable, ReplaySubject } from 'rxjs'
import {
  CurvaAbcGrupo,
  Direction,
  Orcamento,
  Projeto,
  SupplierRule,
} from '../../models'
import { CurvaABCService } from '../../services/orcamento/curva-abc.service'
import {
  debounceTime,
  distinctUntilChanged,
  skip,
  take,
  takeUntil,
} from 'rxjs/operators'
import { cloneDeep, orderBy } from 'lodash'
import { SupplierRulePipe } from '../../shared/pipes/supplier-rule.pipe'
import moment from 'moment'
import { SmoothHeight } from '../../shared/animations/smoothHeight'

@Component({
  selector: 'app-curva-abc',
  templateUrl: `./curva-abc.component.html`,
  styleUrls: ['./curva-abc.component.scss'],
  animations: [SmoothHeight],
})
export class CurvaABCComponent implements OnInit, OnDestroy, AfterViewInit {
  projeto$: Observable<Projeto> = this.orcamentoService.projeto$

  orcamento: Orcamento = this.orcamentoService.orcamento$.value

  grupos$: Observable<CurvaAbcGrupo[]> = this.curvaABCService.grupos$

  headerTable = [
    'codigoGrupo',
    'descricaoFamilia',
    'escopoEntregue',
    'custoEntregue',
    'valorMetaGrupo',
    'valorSelecionado',
    'diff',
    'nomeFantasia',
    'estimatedValue',
    'totalValue',
    'collapse',
  ]

  dataSource: BehaviorSubject<CurvaAbcGrupo[]> = new BehaviorSubject<
    CurvaAbcGrupo[]
  >([])

  sortKey$ = new BehaviorSubject<string>('valorConsiderado')

  sortDirection$ = new BehaviorSubject<Direction>(Direction.DESC)

  directionEnum: typeof Direction = Direction

  expandedElement: CurvaAbcGrupo

  expandedDetailGrupoElement: CurvaAbcGrupo

  totalGoalValue = 0
  totalEstimatedValue = 0

  supplierRulePipe = new SupplierRulePipe()

  consistencyInfo: {
    meta: number
    especialistas: number
    fornecedores: number
  } = {
    meta: 0,
    especialistas: 0,
    fornecedores: 0,
  }

  expandedGroupElement: CurvaAbcGrupo

  @ViewChildren('headerCellElement') cdkHeader: QueryList<
    ElementRef<HTMLTableHeaderCellElement>
  >

  tableHeaderElementsWidthMap: { [key: string]: number }

  popoverFiltersContext = {
    filters: {
      escopoEntregue: {
        emDia: null,
        atrasado: null,
        recebido: null,
      },
      custoEntregue: {
        emDia: null,
        atrasado: null,
        recebido: null,
      },
    },
  }

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>()
  private filtersField$ = new BehaviorSubject<any>(null)

  constructor(
    private orcamentoService: OrcamentoService,
    private curvaABCService: CurvaABCService
  ) {}

  trackById = (index, item: CurvaAbcGrupo) => {
    return item.idGrupo
  }

  ngAfterViewInit(): void {
    this.getTableHeaders()
  }

  calculateTotalAndConsistency() {
    // Calcula o total do valor estimado, valor total da meta dos grupos e a consistência dos grupos
    this.grupos$.pipe(take(1)).subscribe(grupos => {
      grupos.forEach(item => {
        if (item.valorMetaGrupo) {
          this.totalGoalValue += item.valorMetaGrupo
        }
        const rule = this.supplierRulePipe.transform(item)
        if (rule === SupplierRule.META) {
          this.consistencyInfo.meta += 1
        } else if (rule === SupplierRule.ESPECIALISTA) {
          this.consistencyInfo.especialistas += 1
        } else {
          this.consistencyInfo.fornecedores += 1
        }
        this.totalEstimatedValue += item.valorSelecionado || item.valorMetaGrupo
      })
    })
  }

  diffValueFn(goalValue: number, selectedValue: number) {
    if (selectedValue) {
      const diffValue = (goalValue - selectedValue) / goalValue
      return !isNaN(diffValue) ? diffValue : 0
    }
    return 0
  }

  ngOnInit() {
    this.init()
  }

  adjustSort(key: string, direction: Direction) {
    if (
      this.sortKey$.value === key &&
      this.sortDirection$.value === direction
    ) {
      return
    }
    this.sortDirection$.next(direction)
    this.sortKey$.next(key)
  }

  checkDates(date: Date | string) {
    return moment().isAfter(date)
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }

  sumGoal(multiplos: CurvaAbcGrupo[], field: string) {
    return multiplos.reduce((accum, grupo) => accum + grupo[field], 0)
  }

  diffMultiplosValueFn(element: CurvaAbcGrupo) {
    const totalGoal = this.sumGoal(element.multiplos, 'valorMetaGrupo')
    const totalEstimated = this.sumGoal(element.multiplos, 'valorSelecionado')
    return this.diffValueFn(totalGoal, totalEstimated)
  }

  getEstimatedValue(element: CurvaAbcGrupo) {
    if (element.multiplos) {
      return element.multiplos.reduce(
        (accum, grupo) =>
          accum + (grupo.valorSelecionado || grupo.valorMetaGrupo),
        0
      )
    }

    return element.valorSelecionado || element.valorMetaGrupo
  }

  expandElements(element: CurvaAbcGrupo) {
    this.expandedGroupElement =
      this.expandedGroupElement === element ? null : element
  }

  applyFilters(filterField: string) {
    this.filtersField$.next(filterField)
  }

  private init() {
    this.orcamentoService.selectProjetoAction$.next(this.orcamento.idProjeto)
    this.calculateTotalAndConsistency()
    this.sortFilterAndTransformToMultiplos()
  }

  private getTableHeaders() {
    // Pega a referência dos th da tabela e cria um mapa pelo id desses th
    // Usado para calcular o width das tds da sub tabela na da lista dos grupos com multiplos
    this.cdkHeader.changes
      .pipe(
        debounceTime(5),
        skip(1),
        take(1)
      )
      .subscribe(() => {
        const elements: ElementRef<
          HTMLTableHeaderCellElement
        >[] = this.cdkHeader.toArray()
        if (elements.length) {
          this.tableHeaderElementsWidthMap = elements.reduce(
            (accum, element) => {
              accum[element.nativeElement.id] =
                element.nativeElement.offsetWidth
              return accum
            },
            {}
          )
        }
      })
  }

  private sortFilterAndTransformToMultiplos() {
    combineLatest([
      this.grupos$,
      this.sortKey$,
      this.sortDirection$,
      this.filtersField$,
    ])
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(([grupos, sortKey, sortDirection, filtersField]) => {
        const gruposComMultiplos = this.transformToMultiplos(cloneDeep(grupos))

        // Ordena os grupos da tabela com base na chave e a direção passada
        let sortedGroups = orderBy(gruposComMultiplos, sortKey, sortDirection)

        // Filtra por campos da tabela
        if (filtersField) {
          sortedGroups = this.filterBy(filtersField, sortedGroups)
        }

        this.dataSource.next(sortedGroups)
      })
  }

  private filterBy(filtersField, sortedGroups) {
    const { atrasado, emDia, recebido } = this.popoverFiltersContext.filters[
      filtersField
    ]
    sortedGroups = sortedGroups.filter(item =>
      Object.keys(this.popoverFiltersContext.filters[filtersField]).every(
        () => {
          if (filtersField === 'escopoEntregue') {
            if (emDia) {
              return (
                moment().isBefore(item.dataLimiteDefinicao) &&
                !item.escopoEntregue
              )
            }

            if (atrasado) {
              return (
                moment().isAfter(item.dataLimiteDefinicao) &&
                !item.escopoEntregue
              )
            }

            if (recebido) {
              return item.escopoEntregue
            }
          }

          if (filtersField === 'custoEntregue') {
            if (emDia) {
              return (
                moment().isBefore(item.dataLimiteRecebimento) &&
                !item.custoEntregue
              )
            }
            if (atrasado) {
              return (
                moment().isAfter(item.dataLimiteRecebimento) &&
                !item.custoEntregue
              )
            }
            if (recebido) {
              return item.custoEntregue
            }
          }

          return true
        }
      )
    )
    return sortedGroups
  }

  private transformToMultiplos(grupos): CurvaAbcGrupo[] {
    // Adiciona a propriedade multiplos no array de grupos para a funcionalidade
    // de multiplos grupos em diferentes familias
    const ret = grupos.reduce((accum, grupo) => {
      const index = accum.findIndex(g => g.codigoGrupo === grupo.codigoGrupo)
      if (index > -1) {
        accum[index].multiplos = accum[index].multiplos
          ? [...accum[index].multiplos, grupo]
          : [grupo, accum[index]]
      } else {
        accum.push(grupo)
      }
      return accum
    }, [])

    return ret.map(grupo => {
      const valorConsiderado = this.getEstimatedValue(grupo)
      return {
        ...grupo,
        valorConsiderado,
        total: valorConsiderado / this.totalEstimatedValue,
        diff: grupo.multiplos
          ? this.diffMultiplosValueFn(grupo)
          : this.diffValueFn(grupo.valorMetaGrupo, grupo.valorSelecionado),
        regraFornecedor: this.supplierRulePipe.transform(grupo),
      } as CurvaAbcGrupo
    })
  }
}
