import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CurvaABCRoutingModule } from './curva-abc-routing.module'
import { CurvaABCComponent } from './curva-abc.component'
import { SharedModule } from '../../shared/shared.module'
import { HeaderCurvaComponent } from './header-curva/header-curva.component'
import { PropostaComponent } from './proposta/proposta.component'
import { StatusBudgetValuePipe } from './proposta/status-budget-value.pipe'

@NgModule({
  declarations: [
    CurvaABCComponent,
    HeaderCurvaComponent,
    PropostaComponent,
    StatusBudgetValuePipe,
  ],
  imports: [CommonModule, CurvaABCRoutingModule, SharedModule],
  providers: [],
})
export class CurvaABCModule {}
