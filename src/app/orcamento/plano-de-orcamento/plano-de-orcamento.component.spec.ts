import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { PlanoDeOrcamentoComponent } from './plano-de-orcamento.component'

describe('PlanoDeOrcamentoComponent', () => {
  let component: PlanoDeOrcamentoComponent
  let fixture: ComponentFixture<PlanoDeOrcamentoComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanoDeOrcamentoComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanoDeOrcamentoComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
