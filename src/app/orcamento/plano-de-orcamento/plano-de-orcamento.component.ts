import { Component, OnInit } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { Orcamento, Projeto } from '../../models'
import { OrcamentoService } from '../../services/orcamento/orcamento.service'
import { delay, filter } from 'rxjs/operators'
import { FadeOutAnimation } from '../../shared/animations/fadeOut'
import { NavigationEnd, Router } from '@angular/router'

@Component({
  selector: 'app-plano-de-orcamento',
  templateUrl: './plano-de-orcamento.component.html',
  styleUrls: ['./plano-de-orcamento.component.scss'],
  animations: [FadeOutAnimation],
})
export class PlanoDeOrcamentoComponent implements OnInit {
  orcamento: Orcamento = this.orcamentoService.orcamento$.value
  orcamento$: BehaviorSubject<Orcamento> = this.orcamentoService.orcamento$
  projeto$: Observable<Projeto> = this.orcamentoService.projeto$
  soma$: Observable<number> = this.orcamentoService.soma$.pipe(delay(0))

  isVisible = this.router.url.includes('valores')

  constructor(
    public orcamentoService: OrcamentoService,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.isVisible = event.url.includes('valores')
      })
    this.orcamentoService.selectProjetoAction$.next(this.orcamento.idProjeto)
  }
}
