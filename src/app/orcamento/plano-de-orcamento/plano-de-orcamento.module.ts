import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { PlanoDeOrcamentoRoutingModule } from './plano-de-orcamento-routing.module'
import { PlanoDeOrcamentoComponent } from './plano-de-orcamento.component'
import { SharedModule } from '../../shared/shared.module'
import { TabsStepsComponent } from './tabs-steps/tabs-steps.component'
import { TabDatasComponent } from './tabs-steps/tab-datas/tab-datas.component'
import { ModalNovaFamiliaComponent } from './tabs-steps/tab-grupos/modal-nova-familia/modal-nova-familia.component'
import { ListaResponsaveisComponent } from './tabs-steps/tab-responsaveis/lista-responsaveis/lista-responsaveis.component'
import { TabGruposComponent } from './tabs-steps/tab-grupos/tab-grupos.component'
import { TabResponsaveisComponent } from './tabs-steps/tab-responsaveis/tab-responsaveis.component'
import { TabEstrategiaComponent } from './tabs-steps/tab-estrategia/tab-estrategia.component'
import { TabValoresComponent } from './tabs-steps/tab-valores/tab-valores.component'
import { TabFornecedoresComponent } from './tabs-steps/tab-fornecedores/tab-fornecedores.component'
import { ModalFornecedoresComponent } from './tabs-steps/tab-fornecedores/modal-fornecedores/modal-fornecedores.component'
import { ModalSelecionarProjetosComponent } from './tabs-steps/tab-valores/modal-selecionar-projetos/modal-selecionar-projetos.component'

@NgModule({
  entryComponents: [
    ModalNovaFamiliaComponent,
    ListaResponsaveisComponent,
    ModalFornecedoresComponent,
    ModalSelecionarProjetosComponent,
  ],
  declarations: [
    PlanoDeOrcamentoComponent,
    TabsStepsComponent,
    TabDatasComponent,
    ModalNovaFamiliaComponent,
    ListaResponsaveisComponent,
    TabGruposComponent,
    TabResponsaveisComponent,
    TabEstrategiaComponent,
    TabValoresComponent,
    TabFornecedoresComponent,
    ModalFornecedoresComponent,
    ModalSelecionarProjetosComponent,
  ],
  exports: [TabsStepsComponent],
  imports: [CommonModule, PlanoDeOrcamentoRoutingModule, SharedModule],
})
export class PlanoDeOrcamentoModule {}
