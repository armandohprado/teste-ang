import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { PlanoDeOrcamentoComponent } from './plano-de-orcamento.component'
import { TabDatasComponent } from './tabs-steps/tab-datas/tab-datas.component'
import { TabGruposComponent } from './tabs-steps/tab-grupos/tab-grupos.component'
import { TabResponsaveisComponent } from './tabs-steps/tab-responsaveis/tab-responsaveis.component'
import { TabFornecedoresComponent } from './tabs-steps/tab-fornecedores/tab-fornecedores.component'
import { TabValoresComponent } from './tabs-steps/tab-valores/tab-valores.component'
import { TabsGuard } from '../../services/guards/tabs.guard'
import { TabEstrategiaComponent } from './tabs-steps/tab-estrategia/tab-estrategia.component'

const routes: Routes = [
  {
    path: '',
    component: PlanoDeOrcamentoComponent,
    data: {
      breadcrumb: 'plano de orçamento',
    },
    children: [
      {
        path: '',
        component: TabDatasComponent,
      },
      {
        path: 'grupos',
        component: TabGruposComponent,
      },
      {
        path: 'responsaveis',
        canActivate: [TabsGuard],
        component: TabResponsaveisComponent,
      },
      {
        path: 'fornecedores',
        canActivate: [TabsGuard],
        component: TabFornecedoresComponent,
      },
      {
        path: 'estrategias',
        canActivate: [TabsGuard],
        component: TabEstrategiaComponent,
      },
      {
        path: 'valores',
        canActivate: [TabsGuard],
        component: TabValoresComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanoDeOrcamentoRoutingModule {}
