import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabEstrategiaComponent } from './tab-estrategia.component'

describe('TabEstrategiaComponent', () => {
  let component: TabEstrategiaComponent
  let fixture: ComponentFixture<TabEstrategiaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabEstrategiaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabEstrategiaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
