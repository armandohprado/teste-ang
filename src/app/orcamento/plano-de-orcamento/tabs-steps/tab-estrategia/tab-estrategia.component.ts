import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { BehaviorSubject, Observable, ReplaySubject, timer } from 'rxjs'
import { Orcamento, Projeto } from '../../../../models'
import { switchMap, take } from 'rxjs/operators'
import { FadeOutAnimation } from '../../../../shared/animations/fadeOut'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { unMaskMoney } from '../../../../utils'
import { CurrencyPipe } from '@angular/common'

@Component({
  selector: 'app-tab-estrategia',
  templateUrl: './tab-estrategia.component.html',
  styleUrls: ['./tab-estrategia.component.scss'],
  animations: [FadeOutAnimation],
})
export class TabEstrategiaComponent implements OnInit, OnDestroy {
  @HostBinding('classList')
  tab = 'tab-pane container-fluid'
  orcamento$: BehaviorSubject<Orcamento> = this.orcamentoService.orcamento$
  custoMargem: number
  projeto$: Observable<Projeto> = this.orcamentoService.projeto$
  valorPlanejadoVenda: string = new CurrencyPipe('pt-BR').transform(
    this.orcamento$.value.valorPlanejadoVenda,
    ' '
  )
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>()

  constructor(
    public orcamentoService: OrcamentoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  @HostBinding('class.active')
  get url() {
    return this.router.url.includes('estrategias')
  }

  onKey() {
    const valorVenda = unMaskMoney(this.valorPlanejadoVenda)

    if (valorVenda) {
      this.projeto$.pipe(take(1)).subscribe(projeto => {
        this.custoMargem =
          valorVenda * (projeto.percentualMargemContribuicao / 100)
        this.orcamento$.value.valorCustoMeta = valorVenda - this.custoMargem
        this.orcamento$.value.valorPlanejadoVenda = valorVenda
      })
    } else {
      this.orcamento$.value.valorCustoMeta = null
      this.custoMargem = null
    }
  }

  ngOnInit() {
    timer(0)
      .pipe(take(1))
      .subscribe(() => this.onKey())
  }

  save(next?: boolean) {
    if (this.orcamento$.value.valorCustoMeta) {
      this.orcamentoService
        .updatePlanoOrcamento()
        .pipe(
          switchMap(() => this.orcamentoService.refreshOrcamento()),
          take(1)
        )
        .subscribe(() => {
          if (next) {
            this.router.navigate(['../valores'], {
              relativeTo: this.activatedRoute,
            })
          }
        })
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
