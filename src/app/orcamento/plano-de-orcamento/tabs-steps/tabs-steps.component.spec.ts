import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabsStepsComponent } from './tabs-steps.component'

describe('TabsStepsComponent', () => {
  let component: TabsStepsComponent
  let fixture: ComponentFixture<TabsStepsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabsStepsComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsStepsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
