import { Component, HostBinding, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { Observable } from 'rxjs'
import { Familia, Fornecedor, Grupo } from '../../../../models'
import { map, take } from 'rxjs/operators'
import { FornecedorService } from '../../../../services/orcamento/fornecedor.service'
import { BsModalService } from 'ngx-bootstrap'
import { ModalFornecedoresComponent } from './modal-fornecedores/modal-fornecedores.component'

@Component({
  selector: 'app-tab-fornecedores',
  templateUrl: './tab-fornecedores.component.html',
  styleUrls: ['./tab-fornecedores.component.scss'],
})
export class TabFornecedoresComponent implements OnInit {
  @HostBinding('class.tab-pane')
  tab = true
  familias$: Observable<Familia[]> = this.orcamentoService.familias$.pipe(
    map(familias => familias.filter(f => f.grupoes && f.grupoes.length))
  )
  collapses: any = {
    isOpen_0: true,
  }

  constructor(
    private router: Router,
    private orcamentoService: OrcamentoService,
    private fornecedorService: FornecedorService,
    private modalService: BsModalService
  ) {}

  @HostBinding('class.active')
  get url() {
    return this.router.url.includes('fornecedores')
  }

  ngOnInit() {}

  openFornecedoresModal(grupo: Grupo) {
    this.modalService.show(ModalFornecedoresComponent, {
      class: 'modal-lg modal-fornecedores',
      ignoreBackdropClick: true,
      initialState: {
        grupo,
      },
    })
  }

  setFav(idOrcamentoGrupo: number, fornecedor: Fornecedor) {
    fornecedor.favorito = !fornecedor.favorito
    this.fornecedorService
      .updateFornecedorGrupo(
        this.orcamentoService.orcamento$.value.idOrcamento,
        idOrcamentoGrupo,
        fornecedor
      )
      .pipe(take(1))
      .subscribe()
  }
}
