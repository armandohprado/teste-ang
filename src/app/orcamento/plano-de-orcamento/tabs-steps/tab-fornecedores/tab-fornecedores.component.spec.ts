import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabFornecedoresComponent } from './tab-fornecedores.component'

describe('TabFornecedoresComponent', () => {
  let component: TabFornecedoresComponent
  let fixture: ComponentFixture<TabFornecedoresComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabFornecedoresComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabFornecedoresComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
