import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { BsModalRef } from 'ngx-bootstrap'
import { Fornecedor, Grupo, SituacaoFornecedor } from '../../../../../models'
import { switchMap, take, takeUntil, map } from 'rxjs/operators'
import { FornecedorService } from '../../../../../services/orcamento/fornecedor.service'
import { OrcamentoService } from '../../../../../services/orcamento/orcamento.service'
import { Subject } from 'rxjs'
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-modal-fornecedores',
  templateUrl: './modal-fornecedores.component.html',
  styleUrls: ['./modal-fornecedores.component.scss'],
})
export class ModalFornecedoresComponent implements OnInit, OnDestroy {
  situacaoFornecedor: typeof SituacaoFornecedor = SituacaoFornecedor

  filtersForm: FormGroup = this.fb.group({
    busca: this.fb.control(''),
    situacaoFornecedorEnum: this.fb.control(this.situacaoFornecedor.HOMOLOGADO),
  })
  grupo: Grupo
  fornecedores: Fornecedor[]
  fornecedoresSelecionados: Fornecedor[] = []
  private destroyed$: Subject<boolean> = new Subject<boolean>()

  constructor(
    private bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private fornecedorService: FornecedorService,
    private orcamentoService: OrcamentoService,
    private sanitizer: DomSanitizer
  ) {}

  get mensagem() {
    let situacao

    switch (this.filtersForm.get('situacaoFornecedorEnum').value) {
      case this.situacaoFornecedor.HOMOLOGADO:
        situacao = `homologados no grupo`
        break
      case this.situacaoFornecedor.OUTROGRUPO:
        situacao = `homologados na EAP`
        break
      case this.situacaoFornecedor.SIMPLESFORNECEDOR:
        situacao = `cadastrados`
        break
    }

    return this.sanitizer
      .bypassSecurityTrustHtml(`Não existem fornecedores ${situacao} <br/>
          Cadastre na Organização de Projeto`)
  }

  ngOnInit(): void {
    this.fornecedoresSelecionados = (this.grupo.fornecedores || []).slice(0)

    this.getFornecedores(this.situacaoFornecedor.HOMOLOGADO)

    this.filtersForm
      .get('situacaoFornecedorEnum')
      .valueChanges.pipe(takeUntil(this.destroyed$))
      .subscribe(valor => this.getFornecedores(valor))
  }

  getFornecedores(situacao: SituacaoFornecedor) {
    this.fornecedorService
      .getTodosFornecedoresDoGrupo(
        this.orcamentoService.orcamento$.value.idOrcamento,
        this.grupo.idGrupo,
        situacao
      )
      .pipe(take(1))
      .subscribe(fornecedores => (this.fornecedores = fornecedores))
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }

  addFornecedores() {
    this.fornecedorService
      .addFornecedoresGrupo(
        this.orcamentoService.orcamento$.value.idOrcamento,
        this.grupo.idOrcamentoGrupo,
        this.fornecedoresSelecionados
      )
      .pipe(map(() => this.orcamentoService.refreshOrcamento(), take(1)))
      .subscribe(() => this.close())
  }

  close(): void {
    this.bsModalRef.hide()
  }
}
