import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabGruposComponent } from './tab-grupos.component'

describe('TabGruposComponent', () => {
  let component: TabGruposComponent
  let fixture: ComponentFixture<TabGruposComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabGruposComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabGruposComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
