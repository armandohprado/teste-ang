import { Component, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { OrcamentoService } from '../../../../../services/orcamento/orcamento.service'
import { switchMap, take } from 'rxjs/operators'
import { Familia } from '../../../../../models'

@Component({
  selector: 'app-modal-nova-familia',
  templateUrl: './modal-nova-familia.component.html',
  styleUrls: ['./modal-nova-familia.component.scss'],
})
export class ModalNovaFamiliaComponent implements OnInit {
  familia: Familia

  familiaForm: FormGroup = this.fb.group({
    nomeFamiliaCustomizada: this.fb.control('', Validators.required),
  })

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private orcamentoService: OrcamentoService
  ) {}

  ngOnInit() {
    if (this.familia) {
      this.familiaForm.setValue({
        nomeFamiliaCustomizada: this.familia.descricaoFamilia,
      })
    }
  }

  addFamilia() {
    if (this.familiaForm.valid) {
      const payload = this.familia
        ? {
            ...this.familia,
            nomeFamiliaCustomizada: this.familiaForm.value
              .nomeFamiliaCustomizada,
          }
        : this.familiaForm.value

      this.orcamentoService
        .salvarFamilia(payload)
        .pipe(
          switchMap(() => this.orcamentoService.buscarFamilias()),
          switchMap(() => {
            return this.orcamentoService.refreshOrcamento()
          }),
          take(1)
        )
        .subscribe(() => {
          this.close()
        })
    }
  }

  close() {
    this.bsModalRef.hide()
  }
}
