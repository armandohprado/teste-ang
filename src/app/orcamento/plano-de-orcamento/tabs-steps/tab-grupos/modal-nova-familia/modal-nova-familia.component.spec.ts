import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ModalNovaFamiliaComponent } from './modal-nova-familia.component'

describe('ModalNovaFamiliaComponent', () => {
  let component: ModalNovaFamiliaComponent
  let fixture: ComponentFixture<ModalNovaFamiliaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalNovaFamiliaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNovaFamiliaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
