import { Component, HostBinding, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { Familia } from '../../../../models'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { ModalNovaFamiliaComponent } from './modal-nova-familia/modal-nova-familia.component'
import { ActivatedRoute, Router } from '@angular/router'
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop'
import { switchMap, take } from 'rxjs/operators'

@Component({
  selector: 'app-tab-grupos',
  templateUrl: './tab-grupos.component.html',
  styleUrls: ['./tab-grupos.component.scss'],
})
export class TabGruposComponent implements OnInit {
  @HostBinding('class.tab-pane')
  tab = true
  familias$: Observable<Familia[]> = this.orcamentoService.familias$
  private modalRef: BsModalRef

  constructor(
    private modalService: BsModalService,
    public orcamentoService: OrcamentoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  @HostBinding('class.active')
  get url() {
    return this.router.url.includes('grupos')
  }

  trackByFn: any = (familia: Familia) => familia.idFamilia

  ngOnInit() {}

  criarNovaFamilia() {
    this.modalRef = this.modalService.show(ModalNovaFamiliaComponent, {
      class: 'modal-sm modal-dialog-centered modal-nova-familia',
      ignoreBackdropClick: true,
    })
  }

  drop(event: CdkDragDrop<Familia[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      )
    } else {
      this.familias$
        .pipe(take(1))
        .subscribe(familias =>
          moveItemInArray(familias, event.previousIndex, event.currentIndex)
        )
    }
  }

  salvaOrdem(next?: boolean) {
    this.familias$
      .pipe(
        switchMap(familias => this.orcamentoService.ordenaFamilias(familias)),
        take(1)
      )
      .subscribe(() => {
        if (next) {
          this.router.navigate(['../responsaveis'], {
            relativeTo: this.activatedRoute,
          })
        }
      })
  }
}
