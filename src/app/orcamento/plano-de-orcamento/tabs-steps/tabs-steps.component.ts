import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-tabs-steps',
  templateUrl: './tabs-steps.component.html',
  styleUrls: ['./tabs-steps.component.scss'],
})
export class TabsStepsComponent implements OnInit, OnDestroy {
  @HostBinding('class')
  tabsSteps = 'tabs-steps'

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>()

  constructor(public orcamentoService: OrcamentoService) {}

  ngOnInit() {}

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
