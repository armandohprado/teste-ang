import { Component, HostBinding, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Orcamento } from '../../../../models'
import { ActivatedRoute, Router } from '@angular/router'
import { Datas } from '../../../../models/datas'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { switchMap, take } from 'rxjs/operators'
import { ValidationService } from '../../../../services/core/validation.service'

@Component({
  selector: 'app-tab-datas',
  templateUrl: './tab-datas.component.html',
  styleUrls: ['./tab-datas.component.scss'],
})
export class TabDatasComponent implements OnInit {
  @HostBinding('class.tab-pane')
  tab = true
  bsConfig = {
    isAnimated: true,
    containerClass: 'theme-primary',
    dateInputFormat: 'DD/MM/YYYY [às] HH:mm',
  }
  orcamento: Orcamento = this.orcamentoService.orcamento$.value
  datasForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public orcamentoService: OrcamentoService,
    private router: Router
  ) {}

  @HostBinding('class.active')
  get url() {
    return this.router.url.includes('')
  }

  ngOnInit() {
    this.createForm()
  }

  createForm() {
    const {
      dataLimiteApresentacaoCliente,
      dataLimiteAprovacaoCeo,
      dataRecebimentoTodosCustos,
    }: Datas = this.orcamento.datas

    this.datasForm = this.fb.group({
      dataRecebimentoTodosCustos: this.fb.control(
        dataRecebimentoTodosCustos ? new Date(dataRecebimentoTodosCustos) : '',
        ValidationService.dateValidator
      ),
      dataLimiteAprovacaoCeo: this.fb.control(
        dataLimiteAprovacaoCeo ? new Date(dataLimiteAprovacaoCeo) : '',
        ValidationService.dateValidator
      ),
      dataLimiteApresentacaoCliente: this.fb.control(
        dataLimiteApresentacaoCliente
          ? new Date(dataLimiteApresentacaoCliente)
          : '',
        ValidationService.dateValidator
      ),
    })
  }

  saveDatas(next?: boolean) {
    const { valid, value } = this.datasForm
    if (valid && Object.values<Datas>(value).length) {
      this.orcamentoService
        .saveDatas(value)
        .pipe(
          switchMap(() => this.orcamentoService.refreshOrcamento()),
          take(1)
        )
        .subscribe(() => {
          if (next) {
            this.router.navigate(['../grupos'], {
              relativeTo: this.activatedRoute,
            })
          }
        })
    }
  }
}
