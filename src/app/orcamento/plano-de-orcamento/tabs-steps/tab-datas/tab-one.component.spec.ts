import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabDatasComponent } from './tab-datas.component'

describe('TabOneComponent', () => {
  let component: TabDatasComponent
  let fixture: ComponentFixture<TabDatasComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabDatasComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabDatasComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
