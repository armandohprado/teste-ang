import { Component, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'

@Component({
  selector: 'app-modal-selecionar-projetos',
  templateUrl: './modal-selecionar-projetos.component.html',
  styleUrls: ['./modal-selecionar-projetos.component.scss'],
})
export class ModalSelecionarProjetosComponent implements OnInit {
  constructor(public bsModalRef: BsModalRef) {}

  close() {
    this.bsModalRef.hide()
  }

  ngOnInit() {}
}
