import { Component, HostBinding, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { DisplayGruposBy, Grupo, Orcamento } from '../../../../models'
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs'
import { map } from 'rxjs/operators'
import { BsModalService } from 'ngx-bootstrap'
import { unMaskMoney } from '../../../../utils'
import { DecimalPipe } from '@angular/common'
import { ModalSelecionarProjetosComponent } from './modal-selecionar-projetos/modal-selecionar-projetos.component'
import { FadeOutAnimation } from '../../../../shared/animations/fadeOut'

@Component({
  selector: 'app-tab-valores',
  templateUrl: './tab-valores.component.html',
  styleUrls: ['./tab-valores.component.scss'],
  animations: [FadeOutAnimation],
})
export class TabValoresComponent implements OnInit {
  constructor(
    private route: Router,
    private orcamentoService: OrcamentoService,
    private modalService: BsModalService
  ) {}

  @HostBinding('class.tab-pane') tab = true
  @HostBinding('class.active') get url() {
    return this.route.url.includes('valores')
  }

  private TEMPORARY_PERCENTAGE = 0.05
  private currency = new DecimalPipe('pt-BR')
  private triggerUpdate: BehaviorSubject<number> = new BehaviorSubject<number>(
    null
  )

  optionTypes = Object.values(DisplayGruposBy)
  selectedType = DisplayGruposBy.GRUPO
  orcamento: Orcamento = this.orcamentoService.orcamento$.value
  displayBy: BehaviorSubject<string> = new BehaviorSubject<string>(
    DisplayGruposBy.GRUPO
  )

  collapses: any = {
    isOpen_0: true,
  }

  soma$: Subject<number> = this.orcamentoService.soma$
  items$: Observable<any[]> = combineLatest([
    this.orcamentoService.familias$,
    this.triggerUpdate,
    this.displayBy,
  ]).pipe(
    map(([familias, triggerUpdate, displayBy]) => {
      const filteredFamilies = familias.filter(
        f => f.grupoes && f.grupoes.length
      )
      let mergedGrupos = []
      let soma = 0
      filteredFamilies.forEach(({ grupoes }) => {
        grupoes.forEach(({ grupos }) => {
          grupos.forEach(grupo => {
            if (!triggerUpdate) {
              grupo.valorMetaGrupo = this.currency.transform(
                this.TEMPORARY_PERCENTAGE * this.orcamento.valorCustoMeta,
                `1.2`
              ) as any
              soma += this.TEMPORARY_PERCENTAGE * this.orcamento.valorCustoMeta
            } else {
              if (grupo.valorMetaGrupo) {
                soma += unMaskMoney(grupo.valorMetaGrupo)
              }
            }
          })
          if (displayBy !== DisplayGruposBy.GRUPO) {
            mergedGrupos = mergedGrupos.concat(grupos)
          }
        })
      })
      this.orcamentoService.soma$.next(soma)
      if (displayBy === DisplayGruposBy.GRUPO) {
        return familias
      }
      return mergedGrupos
    })
  )

  trackByFn: any = (grupo: Grupo) => grupo.idGrupo

  onKey() {
    this.triggerUpdate.next(1)
  }

  openProjetosModal() {
    this.modalService.show(ModalSelecionarProjetosComponent, {
      ignoreBackdropClick: true,
      class: 'modal-lg',
    })
  }

  onSelect() {
    this.displayBy.next(this.selectedType)
  }

  ngOnInit() {}
}
