import { Component, HostBinding, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs'
import {
  Familia,
  FlagResponsavelEnum,
  Grupo,
  Projeto,
  TipoResponsavel,
} from '../../../../models'
import { OrcamentoService } from '../../../../services/orcamento/orcamento.service'
import { FormBuilder, NgForm } from '@angular/forms'
import { map, switchMap, take } from 'rxjs/operators'
import { ListaResponsaveisComponent } from './lista-responsaveis/lista-responsaveis.component'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { ResponsavelService } from '../../../../services/orcamento/responsavel.service'
import { markFormAs } from '../../../../utils'

@Component({
  selector: 'app-tab-responsaveis',
  templateUrl: './tab-responsaveis.component.html',
  styleUrls: ['./tab-responsaveis.component.scss'],
})
export class TabResponsaveisComponent implements OnInit {
  @HostBinding('class.tab-pane')
  tab = true
  familias$: Observable<Familia[]> = this.orcamentoService.familias$.pipe(
    map(familias => this.fillDataLimiteCustos(familias))
  )
  projeto$: Observable<Projeto> = this.orcamentoService.projeto$

  collapses: any = {
    isOpen_0: true,
  }

  modalRef: BsModalRef

  bsConfig = {
    isAnimated: true,
    containerClass: 'theme-primary',
    dateInputFormat: 'DD/MM/YYYY [às] HH:mm',
  }

  flagResponsavel: typeof FlagResponsavelEnum = FlagResponsavelEnum

  tipoResponsavel: typeof TipoResponsavel = TipoResponsavel

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public orcamentoService: OrcamentoService,
    private fb: FormBuilder,
    private modalService: BsModalService,
    private responsavelService: ResponsavelService
  ) {}

  @HostBinding('class.active')
  get url() {
    return this.router.url.includes('responsaveis')
  }

  trackGrupaoFn = grupao => grupao.idGrupao

  trackGrupoFn = grupo => grupo.idGrupo

  trackByFn = responsavel => responsavel.idFuncionario

  openModal(grupo: Grupo, flagResponsavel: number, tipoResponsavel: number) {
    // receber a categoria e filtrar a query para Backend
    this.responsavelService
      .getResponsaveis(
        this.orcamentoService.orcamento$.value.idOrcamento,
        grupo.idGrupo,
        flagResponsavel
      )
      .pipe(take(1))
      .subscribe(responsaveis => {
        this.modalRef = this.modalService.show(ListaResponsaveisComponent, {
          class: 'modal-md modal-lista-responsaveis modal-dialog-centered',
          initialState: {
            responsaveis,
            grupo,
            flagResponsavel,
            tipoResponsavel,
          },
          ignoreBackdropClick: false,
        })
      })
  }

  ngOnInit() {}

  trackByFnFamilia(familia) {
    return familia.idFamilia
  }

  onSubmit(responsaveisForm: NgForm, next?: boolean) {
    if (responsaveisForm.valid) {
      this.orcamentoService
        .saveGrupos()
        .pipe(
          switchMap(() => this.orcamentoService.refreshOrcamento()),
          take(1)
        )
        .subscribe(() => {
          if (next) {
            this.router.navigate(['../fornecedores'], {
              relativeTo: this.activatedRoute,
            })
          }
        })
    } else {
      markFormAs(responsaveisForm.form)
    }
  }

  private fillDataLimiteCustos(familias: Familia[]): Familia[] {
    const {
      dataRecebimentoTodosCustos,
    } = this.orcamentoService.orcamento$.value.datas

    familias.forEach(familia =>
      familia.grupoes.forEach(grupao =>
        grupao.grupos.forEach(grupo => {
          const { dataLimiteDefinicao, dataLimiteRecebimento } = grupo
          if (dataLimiteDefinicao) {
            grupo.dataLimiteDefinicao = new Date(dataLimiteDefinicao)
          }
          if (dataLimiteRecebimento) {
            grupo.dataLimiteRecebimento = new Date(dataLimiteRecebimento)
          } else if (!dataLimiteRecebimento && dataRecebimentoTodosCustos) {
            grupo.dataLimiteRecebimento = new Date(dataRecebimentoTodosCustos)
          }
        })
      )
    )
    return familias.filter(familia => familia.grupoes && familia.grupoes.length)
  }
}
