import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TabResponsaveisComponent } from './tab-responsaveis.component'

describe('TabResponsaveisComponent', () => {
  let component: TabResponsaveisComponent
  let fixture: ComponentFixture<TabResponsaveisComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabResponsaveisComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TabResponsaveisComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
