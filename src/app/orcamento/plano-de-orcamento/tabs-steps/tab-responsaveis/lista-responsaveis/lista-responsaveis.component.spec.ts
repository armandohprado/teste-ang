import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ListaResponsaveisComponent } from './lista-responsaveis.component'

describe('ListaResponsaveisComponent', () => {
  let component: ListaResponsaveisComponent
  let fixture: ComponentFixture<ListaResponsaveisComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListaResponsaveisComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaResponsaveisComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
