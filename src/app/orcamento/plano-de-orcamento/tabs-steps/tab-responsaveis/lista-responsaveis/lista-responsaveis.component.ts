import { Component, OnDestroy, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap/modal'
import {
  FlagResponsavelEnum,
  Funcionario,
  Grupo,
  Responsavel,
} from '../../../../../models'
import { ResponsavelService } from '../../../../../services/orcamento/responsavel.service'
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  take,
  takeUntil,
} from 'rxjs/operators'
import { OrcamentoService } from '../../../../../services/orcamento/orcamento.service'
import { Observable, Subject } from 'rxjs'
import { environment } from '../../../../../../environments/environment'

@Component({
  selector: 'app-lista-responsaveis',
  templateUrl: './lista-responsaveis.component.html',
  styleUrls: ['./lista-responsaveis.component.scss'],
})
export class ListaResponsaveisComponent implements OnInit, OnDestroy {
  responsaveis: Responsavel[]
  grupo: Grupo
  flagResponsavel: number
  tipoResponsavel: number
  showAll: boolean
  busca: string

  get fotoUrl() {
    return environment.fotoUrl
  }

  onSearchChanged: Subject<string> = new Subject<string>()
  private destroyed$: Subject<boolean> = new Subject<boolean>()

  constructor(
    private bsModalRef: BsModalRef,
    private responsavelService: ResponsavelService,
    private orcamentoService: OrcamentoService
  ) {}

  ngOnInit() {
    this.onSearchChanged
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe(term => this.getResponsaveis(term))
  }

  close() {
    this.bsModalRef.hide()
  }

  select(responsavel: Responsavel) {
    if (responsavel) {
      this.responsavelService
        .saveResponsavel({
          idFuncionario: responsavel.idFuncionario,
          idTipoResponsavel: this.tipoResponsavel,
          principal:
            FlagResponsavelEnum.Go === this.flagResponsavel ||
            FlagResponsavelEnum.Vertical === this.flagResponsavel ||
            FlagResponsavelEnum.Gpp === this.flagResponsavel,
          idOrcamentoGrupo: this.grupo.idOrcamentoGrupo,
        })
        .pipe(
          take(1),
          switchMap(() => this.orcamentoService.refreshOrcamento())
        )
        .subscribe(() => this.close())
    }
  }

  getResponsaveis(term?: string) {
    let buscaObservable: Observable<Funcionario[]> = new Observable<
      Funcionario[]
    >()

    if (term && this.showAll) {
      buscaObservable = this.responsavelService.getResponsaveis(
        this.orcamentoService.orcamento$.value.idOrcamento,
        this.grupo.idGrupo,
        null,
        this.busca
      )
    } else if (!this.showAll) {
      buscaObservable = this.responsavelService.getResponsaveis(
        this.orcamentoService.orcamento$.value.idOrcamento,
        this.grupo.idGrupo,
        this.flagResponsavel
      )
    }
    buscaObservable
      .pipe(take(1))
      .subscribe(responsaveis => (this.responsaveis = responsaveis))
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
