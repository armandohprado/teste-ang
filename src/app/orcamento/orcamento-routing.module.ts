import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { OrcamentoResolverService } from '../services/orcamento/orcamento-resolver.service'
import { ProjetoResolverService } from '../services/orcamento/projeto-resolver.service'
import { CotacaoResolverService } from '../services/cotacao/cotacao-resolver.service'

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'configuracoes',
  },
  {
    path: 'configuracoes',
    resolve: {
      projeto: ProjetoResolverService,
    },
    loadChildren: () =>
      import('./configuracao/configuracao.module').then(
        m => m.ConfiguracaoModule
      ),
  },
  {
    path: ':idOrcamento/plano-de-orcamento',
    resolve: {
      orcamento: OrcamentoResolverService,
    },
    loadChildren: () =>
      import('./plano-de-orcamento/plano-de-orcamento.module').then(
        m => m.PlanoDeOrcamentoModule
      ),
  },
  {
    path: ':idOrcamento/curva-abc',
    resolve: {
      orcamento: OrcamentoResolverService,
    },
    loadChildren: () =>
      import('./curva-abc/curva-abc.module').then(m => m.CurvaABCModule),
  },
  {
    path: ':idOrcamento/cotacao',
    resolve: {
      orcamento: CotacaoResolverService,
    },
    loadChildren: () =>
      import('./cotacao/cotacao.module').then(m => m.CotacaoModule),
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrcamentoRoutingModule {}
