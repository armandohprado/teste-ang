import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ConfiguracaoComponent } from './configuracao.component'

const routes: Routes = [
  {
    path: '',
    component: ConfiguracaoComponent,
    data: {
      breadcrumb: 'configuração de orçamento',
      actualPath: 'orcamentos',
    },
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfiguracaoRoutingModule {}
