import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { ConfiguracaoRoutingModule } from './configuracao-routing.module'
import { ConfiguracaoComponent } from './configuracao.component'
import { ModalOrcamentoComponent } from './modal-orcamento/modal-orcamento.component'
import { SharedModule } from '../../shared/shared.module'
import { ProjetoResolverService } from '../../services/orcamento/projeto-resolver.service'

@NgModule({
  entryComponents: [ModalOrcamentoComponent],
  declarations: [ConfiguracaoComponent, ModalOrcamentoComponent],
  imports: [
    CommonModule,
    ConfiguracaoRoutingModule,
    SharedModule,
    RouterModule,
  ],
  exports: [ConfiguracaoComponent],
  providers: [ProjetoResolverService],
})
export class ConfiguracaoModule {}
