import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { OrcamentoService } from '../../services/orcamento/orcamento.service'
import { DeleteModalComponent } from '../../shared/delete-modal/delete-modal.component'
import { Orcamento, Projeto } from '../../models'
import { switchMap, take, takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar'
import { ActivatedRoute } from '@angular/router'
import { ModalOrcamentoComponent } from './modal-orcamento/modal-orcamento.component'

@Component({
  selector: 'app-configuracao',
  templateUrl: `./configuracao.component.html`,
  styleUrls: ['./configuracao.component.scss'],
})
export class ConfiguracaoComponent implements OnInit, OnDestroy {
  bsModalRef: BsModalRef
  projeto: Projeto = this.route.snapshot.data.projeto
  @ViewChild(PerfectScrollbarDirective, { static: true })
  ps: PerfectScrollbarDirective
  private deleteModalRef: BsModalRef
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject()

  constructor(
    private modalService: BsModalService,
    private orcamentoService: OrcamentoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}

  openModalOrcamento(orcamento?: Orcamento) {
    const initialState = {
      projeto: this.projeto,
      orcamento,
      confirmAction: () => this.getProject(true),
    }
    this.bsModalRef = this.modalService.show(ModalOrcamentoComponent, {
      initialState,
      class: 'modal-lg',
      ignoreBackdropClick: true,
    })
  }

  deleteOrcamento(index: number) {
    this.openDeleteModal(index)
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }

  private getProject(hideModal?: boolean) {
    this.orcamentoService
      .retrieveProject(this.projeto.idProjeto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((projeto: Projeto) => {
        this.projeto = projeto
        if (hideModal) {
          this.bsModalRef.hide()
        }
      })
  }

  private openDeleteModal(index: number) {
    this.deleteModalRef = this.modalService.show(DeleteModalComponent, {
      initialState: {
        title: 'excluir orçamento',
        bodyContent: 'Tem certeza que deseja excluir?',
        confirmBtnName: 'excluir',
        closeBtnName: 'não excluir',
        confirmAction: () => this.deleteAction(index),
      },
      class: 'modal-dialog-centered modal-sm delete-modal',
    })
  }

  private deleteAction(index: number) {
    const { idProjeto, orcamentos } = this.projeto
    this.orcamentoService
      .deleteOrcamento(idProjeto, orcamentos[index].idOrcamento)
      .pipe(
        switchMap(() =>
          this.orcamentoService.retrieveProject(this.projeto.idProjeto)
        ),
        take(1)
      )
      .subscribe(res => {
        this.projeto = res
        this.deleteModalRef.hide()
        this.ps.update()
      })
  }
}
