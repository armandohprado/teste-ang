import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms'
import PerfectScrollbar from 'perfect-scrollbar'
import { DOCUMENT } from '@angular/common'
import * as moment from 'moment'
import { Observable } from 'rxjs'
import { Edificio, Fase, Orcamento, Projeto } from '../../../models'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { take } from 'rxjs/operators'
import { ValidationService } from '../../../services/core/validation.service'
import { setPerfectScroll } from '../../../utils'

@Component({
  selector: 'app-modal-orcamento',
  templateUrl: './modal-orcamento.component.html',
  styleUrls: ['./modal-orcamento.component.scss'],
})
export class ModalOrcamentoComponent implements OnInit, OnDestroy {
  buildings: Observable<Edificio[]>
  orcamento: Orcamento
  budgetForm: FormGroup = this.fb.group({
    nomeOrcamento: this.fb.control('', Validators.required),
    edificios: this.fb.control([], Validators.required),
    fases: this.fb.array([], Validators.required),
  })
  projeto: Projeto
  bsConfig = { isAnimated: true, containerClass: 'theme-primary' }

  confirmAction: () => void

  private ps: PerfectScrollbar

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    @Inject(DOCUMENT) private document: Document,
    private configuracaoService: OrcamentoService
  ) {}

  get tooltipText(): string {
    return `Verifique os campos marcados em vermelho no formulário.
  Adicione Fases e Edifícios.`
  }

  get budgetNameField(): FormGroup {
    return this.budgetForm.get('nomeOrcamento') as FormGroup
  }

  get fasesField(): FormArray {
    return this.budgetForm.get('fases') as FormArray
  }

  get fasesControls(): FormGroup[] {
    return this.fasesField.controls as FormGroup[]
  }

  static minDateValidator(control: FormGroup) {
    if (control.parent) {
      return moment(control.value).isBefore(
        control.parent.get('dataInicioFase').value
      )
        ? { minDate: true }
        : null
    }
  }

  initFasesGroup(fase: Fase = {} as Fase): FormGroup {
    const inicio: any = fase.dataInicioFase
      ? moment(fase.dataInicioFase).toDate()
      : ''
    const fim: any = fase.dataFimFase ? moment(fase.dataFimFase).toDate() : ''

    return this.fb.group({
      nomeFase: this.fb.control(fase.nomeFase, Validators.required),
      dataInicioFase: this.fb.control(inicio, [
        Validators.required,
        ValidationService.dateValidator,
      ]),
      dataFimFase: this.fb.control(fim, [
        Validators.required,
        ValidationService.dateValidator,
        ModalOrcamentoComponent.minDateValidator,
      ]),
    })
  }

  ngOnInit(): void {
    this.init()
  }

  addFase() {
    this.fasesField.push(this.initFasesGroup())
    this.fasesField.updateValueAndValidity()
  }

  removeFase(index: number) {
    this.fasesField.removeAt(index)
  }

  saveBuildings($event: any[]) {
    this.budgetForm.get('edificios').setValue($event)
  }

  salvarOrcamento() {
    if (this.budgetForm.valid) {
      const fases = this.budgetForm.value.fases.map(fase => {
        return {
          ...fase,
          dataFimFase: moment(fase.dataFimFase, 'DD/MM/YYYY').toDate(),
          dataInicioFase: moment(fase.dataInicioFase, 'DD/MM/YYYY').toDate(),
        }
      })

      const orcamento = {
        ...(this.orcamento || {}),
        ...this.budgetForm.value,
        fases,
      }

      this.configuracaoService
        .saveOrcamento({
          ...orcamento,
          idProjeto: this.projeto.idProjeto,
        })
        .pipe(take(1))
        .subscribe(() => {
          this.confirmAction()
        })
    }
  }

  ngOnDestroy(): void {
    if (this.ps) {
      this.ps.destroy()
    }
  }

  private init() {
    this.setPS()

    this.getBuildings()

    this.filForm()
  }

  private filForm() {
    if (this.orcamento) {
      const { nomeOrcamento, edificios, fases = [] } = this.orcamento
      this.budgetForm.setValue({
        nomeOrcamento,
        edificios,
        fases: [],
      })

      if (fases.length) {
        fases.forEach(fase => this.fasesField.push(this.initFasesGroup(fase)))
      }
    }
  }

  private getBuildings() {
    this.buildings = this.configuracaoService.retrieveBuildings(
      this.projeto.idProjeto
    )
  }

  private setPS() {
    this.ps = setPerfectScroll(this.document)
  }
}
