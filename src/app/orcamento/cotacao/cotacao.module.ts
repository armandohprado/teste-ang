import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CotacaoRoutingModule } from './cotacao-routing.module'
import { HomeComponent } from './home/home.component'
import { SharedModule } from '../../shared/shared.module'
import { ModalArquivosPropostaComponent } from './home/modal-arquivos-proposta/modal-arquivos-proposta.component'
import { ModalHistoricoPropostaComponent } from './home/modal-historico-proposta/modal-historico-proposta.component'
import { NgxFileDropModule } from 'ngx-file-drop'

import { TypeaheadModule } from 'ngx-bootstrap'
import { ModalEnvioDeCotacaoComponent } from './home/modal-envio-de-cotacao/modal-envio-de-cotacao.component'
import { CKEditorModule } from '@ckeditor/ckeditor5-angular'

import { Step1Component } from './home/modal-envio-de-cotacao/steps/step1/step1.component'
import { Step2Component } from './home/modal-envio-de-cotacao/steps/step2/step2.component'
import { Step3Component } from './home/modal-envio-de-cotacao/steps/step3/step3.component'

@NgModule({
  entryComponents: [
    ModalArquivosPropostaComponent,
    ModalHistoricoPropostaComponent,
    ModalEnvioDeCotacaoComponent,
  ],
  declarations: [
    HomeComponent,
    ModalArquivosPropostaComponent,
    ModalHistoricoPropostaComponent,
    ModalEnvioDeCotacaoComponent,
    Step1Component,
    Step2Component,
    Step3Component,
  ],
  imports: [
    CommonModule,
    CotacaoRoutingModule,
    SharedModule,
    TypeaheadModule,
    NgxFileDropModule,
    CKEditorModule,
  ],
})
export class CotacaoModule {}
