import { Component, OnDestroy, OnInit } from '@angular/core'
import { CotacaoService } from '../../../services/cotacao/cotacao.service'
import { FormControl, FormGroup } from '@angular/forms'

import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { distinctUntilChanged } from 'rxjs/operators'
import { ActivatedRoute } from '@angular/router'
import { FadeOutAnimation } from '../../../shared/animations/fadeOut'
import { BehaviorSubject } from 'rxjs'
import { cloneDeep, uniqBy } from 'lodash'

enum FieldsName {
  Fornecedores = 'fornecedores',
  Grupos = 'grupos',
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [FadeOutAnimation],
})
export class HomeComponent implements OnInit, OnDestroy {
  listaGrupos = []
  listaFornecedores = []

  fieldsEnum: typeof FieldsName = FieldsName

  orcamento = this.route.snapshot.data.orcamento
  familias$: BehaviorSubject<any> = new BehaviorSubject<any>(
    this.orcamento.familias
  )

  collapses: any = {
    isOpen_0: true,
  }

  formSearch = new FormGroup({
    grupos: new FormControl(),
    fornecedores: new FormControl({ disabled: true, value: '' }),
  })

  bsModalRef: BsModalRef

  constructor(
    private modalService: BsModalService,
    private cotacaoService: CotacaoService,
    private route: ActivatedRoute
  ) {}

  get gruposControl(): FormControl {
    return this.formSearch.get('grupos') as FormControl
  }

  get fornecedoresControl(): FormControl {
    return this.formSearch.get('fornecedores') as FormControl
  }

  ngOnInit() {
    this.extractGroups()
    this.gruposControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe(grupos => {
        if (grupos && grupos.length) {
          this.listaFornecedores = grupos.reduce(
            (accum, grupo) => accum.concat(grupo.fornecedores),
            []
          )
          this.listaFornecedores = uniqBy(
            this.listaFornecedores,
            'idFornecedor'
          )
          this.fornecedoresControl.enable()
        } else {
          this.fornecedoresControl.disable()
        }
      })
  }

  ngOnDestroy() {}

  extractGroups() {
    this.listaGrupos = this.orcamento.familias.reduce(
      (accumFam, { grupoes }) =>
        accumFam.concat(
          grupoes.reduce(
            (accGrupoes, { orcamentoGrupos }) =>
              accGrupoes.concat(
                orcamentoGrupos.reduce(
                  (accum, orcamentoGrupo) => accum.concat(orcamentoGrupo.grupo),
                  []
                )
              ),
            []
          )
        ),
      []
    )
  }

  applyFilters() {
    const { value: grupos } = this.gruposControl
    const { value: fornecedores } = this.fornecedoresControl
    let familias = cloneDeep(this.orcamento.familias)
    if (grupos && grupos.length && !fornecedores) {
      familias = familias.filter(familia =>
        grupos.find(
          grupo =>
            !!familia.grupoes.filter(grupao => {
              const filter = grupao.orcamentoGrupos.filter(
                orcamentoGrupo => orcamentoGrupo.grupo.idGrupo === grupo.idGrupo
              )
              return !!filter.length
            }).length
        )
      )
    }
    this.familias$.next(familias)
  }

  clearFilters(field: FieldsName) {
    if (field === FieldsName.Grupos) {
      this.gruposControl.setValue(null)
      this.familias$.next(cloneDeep(this.orcamento.familias))
    } else {
      this.fornecedoresControl.setValue(null)
    }
  }
}
