import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEnvioDeCotacaoComponent } from './modal-envio-de-cotacao.component';

describe('ModalEnvioDeCotacaoComponent', () => {
  let component: ModalEnvioDeCotacaoComponent;
  let fixture: ComponentFixture<ModalEnvioDeCotacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEnvioDeCotacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEnvioDeCotacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
