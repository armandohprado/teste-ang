import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { BsModalRef, TabsetComponent } from 'ngx-bootstrap'
import { DOCUMENT } from '@angular/common'

@Component({
  selector: 'app-modal-envio-de-cotacao',
  templateUrl: './modal-envio-de-cotacao.component.html',
  styleUrls: ['./modal-envio-de-cotacao.component.scss'],
})
export class ModalEnvioDeCotacaoComponent implements OnInit, OnDestroy {
  idOrcamento: number

  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent

  constructor(
    public bsModalRef: BsModalRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit() {
    const element = this.document.querySelector('.modal') as HTMLElement
  }

  ngOnDestroy(): void {}

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true
  }
}
