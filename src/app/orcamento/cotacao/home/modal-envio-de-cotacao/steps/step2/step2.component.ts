import { Component, OnInit } from '@angular/core'
import { ModalEnvioCotacaoStep2 } from '../../../../../../models/modalEnvioCotacaoStep2'
import { EnvioDeCotacaoService } from 'src/app/services/cotacao/envio-de-cotacao.service'
import { take } from 'rxjs/operators'
import { CotacaoService } from '../../../../../../services/cotacao/cotacao.service'

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss'],
})
export class Step2Component implements OnInit {
  lista
  idTest = 2

  constructor(
    private envioService: EnvioDeCotacaoService,
    private cotacaoService: CotacaoService
  ) {}

  ngOnInit() {
    this.cotacaoService.currentId$.subscribe(id => {
      const grupo = this.cotacaoService.GrupoSources.value

      this.envioService
        .getFornecedores(this.idTest, grupo)
        .pipe(take(1))
        .subscribe((data: any) => {
          this.lista = data
          this.selectAll()
        })
    })
  }

  changeItemStatus(item) {
    item.checkedStatus = !item.checkedStatus

    this.envioService
      .setCheckedFornecedor(
        item.idFornecedor,
        this.cotacaoService.currentIdSource.value
      )
      .pipe(take(1))
      .subscribe(data => {
        console.log(data)
      })
  }

  selectAll(starting?) {
    this.lista.fornecedores.forEach(item => {
      item.checkedStatus = !item.checkedStatus
    })

    if (!starting) {
      this.lista.fornecedoresHomologados.forEach(item => {
        item.checkedStatus = !item.checkedStatus
      })
    }
  }
}
