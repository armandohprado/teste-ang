import { Component, OnInit } from '@angular/core'

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { CotacaoService } from '../../../../../../services/cotacao/cotacao.service'
import { take } from 'rxjs/operators'
import { EnvioDeCotacaoService } from '../../../../../../services/cotacao/envio-de-cotacao.service'

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
})
export class Step1Component implements OnInit {
  public Editor = ClassicEditor
  info
  fileData: File = null
  filesLength = 0
  id$ = this.cotacaoService.currentIdSource.value

  constructor(
    private cotacaoService: CotacaoService,
    private envioDeCotacao: EnvioDeCotacaoService
  ) {}

  ngOnInit() {
    this.cotacaoService
      .getEnvioDeCotacaoInfo(this.id$)
      .pipe(take(1))
      .subscribe((data: any) => {
        this.info = data
      })
  }

  fileProgress(fileInput: any) {
    this.fileData = fileInput.target.files

    if (fileInput.target.files.length > 0) {
      this.filesLength = fileInput.target.files.length
      this.envioDeCotacao.sendFile(this.id$, this.fileData).subscribe(
        success => {
          console.log('Sucesso', success)
        },
        err => {
          console.error(err)
        }
      )
    }
  }
}
