import { Component, Input, OnInit } from '@angular/core'
import {
  EnvioDeCotacaoService,
  SitesResponse,
} from '../../../../../../services/cotacao/envio-de-cotacao.service'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss'],
})
export class Step3Component implements OnInit {
  @Input() idOrcamento: number
  sites$: Observable<SitesResponse>

  constructor(private envioDeCotacaoService: EnvioDeCotacaoService) {}

  ngOnInit() {
    this.sites$ = this.envioDeCotacaoService.retrieveSites(this.idOrcamento)
  }
}
