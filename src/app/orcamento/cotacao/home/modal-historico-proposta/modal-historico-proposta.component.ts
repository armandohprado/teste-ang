import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { DOCUMENT } from '@angular/common'
import { CotacaoService } from '../../../../services/cotacao/cotacao.service';

@Component({
  selector: 'app-modal-historico-proposta',
  templateUrl: './modal-historico-proposta.component.html',
  styleUrls: ['./modal-historico-proposta.component.scss'],
})
export class ModalHistoricoPropostaComponent implements OnInit, OnDestroy {

  history;
  listHistory;
  idProposta;

  constructor(
    public bsModalRef: BsModalRef,
    @Inject(DOCUMENT) private document: Document,
    private cotacaoService: CotacaoService
  ) {}

  ngOnInit(): void {
    const element = this.document.querySelector('.modal') as HTMLElement

    this.cotacaoService.idProposta$.subscribe( lista => {
      this.history = this.cotacaoService.getControleCotacaoHistorico(lista.idOrcamento,lista.idOrcamentoGrupo,lista.idProposta,"historico")
      .subscribe( data => {
        this.listHistory = data;
        this.idProposta = lista.idProposta;
      })
    })
  }

  ngOnDestroy(): void {
    this.history.unsubscribe();
  }
}
