import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ModalHistoricoPropostaComponent } from './modal-historico-proposta.component'

describe('ModalHistoricoPropostaComponent', () => {
  let component: ModalHistoricoPropostaComponent
  let fixture: ComponentFixture<ModalHistoricoPropostaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalHistoricoPropostaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHistoricoPropostaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
