import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ModalArquivosPropostaComponent } from './modal-arquivos-proposta.component'

describe('ModalArquivosPropostaComponent', () => {
  let component: ModalArquivosPropostaComponent
  let fixture: ComponentFixture<ModalArquivosPropostaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalArquivosPropostaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalArquivosPropostaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
