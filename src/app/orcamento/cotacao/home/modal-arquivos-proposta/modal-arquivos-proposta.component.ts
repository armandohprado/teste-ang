import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { DOCUMENT } from '@angular/common'
import { CotacaoService } from '../../../../services/cotacao/cotacao.service'
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from 'ngx-file-drop'

@Component({
  selector: 'app-modal-arquivos-proposta',
  templateUrl: './modal-arquivos-proposta.component.html',
  styleUrls: ['./modal-arquivos-proposta.component.scss'],
})
export class ModalArquivosPropostaComponent implements OnInit, OnDestroy {
  public files: NgxFileDropEntry[] = []
  listFiles
  idProposta
  filesSubscribe

  constructor(
    public bsModalRef: BsModalRef,
    @Inject(DOCUMENT) private document: Document,
    private cotacaoService: CotacaoService
  ) {}

  ngOnInit(): void {
    const element = this.document.querySelector('.modal') as HTMLElement

    this.cotacaoService.idProposta$.subscribe(lista => {
      this.filesSubscribe = this.cotacaoService
        .getControleCotacaoHistorico(
          lista.idOrcamento,
          lista.idOrcamentoGrupo,
          lista.idProposta
        )
        .subscribe(
          (data: any) => {
            this.listFiles = data
          },
          err => {
            console.error(err)
          }
        )
    })
  }

  ngOnDestroy(): void {
    this.filesSubscribe.unsubscribe()
  }

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files
    console.log('dropped file')
    console.dir(this.files)
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry
        fileEntry.file((file: File) => {
          // Here you can access the real file
          console.log('is File')
          console.log(droppedFile.relativePath, file)
        })
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry
        console.log(droppedFile.relativePath, fileEntry)
      }
    }
  }

  public fileOver(event) {
    console.log(event)
  }

  public fileLeave(event) {
    console.log(event)
  }
}
