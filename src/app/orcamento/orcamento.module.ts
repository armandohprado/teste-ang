import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { OrcamentoRoutingModule } from './orcamento-routing.module'
import { OrcamentoResolverService } from '../services/orcamento/orcamento-resolver.service'
import { CotacaoResolverService } from '../services/cotacao/cotacao-resolver.service'

@NgModule({
  imports: [CommonModule, OrcamentoRoutingModule],
  providers: [OrcamentoResolverService, CotacaoResolverService],
})
export class OrcamentoModule {}
