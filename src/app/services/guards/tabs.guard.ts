import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router'
import { OrcamentoService } from '../orcamento/orcamento.service'
import { switchMap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class TabsGuard implements CanActivate {
  constructor(private orcamentoService: OrcamentoService) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.orcamentoService.orcamento$.pipe(
      switchMap(orcamento => {
        if (orcamento) {
          return of(!!(orcamento.grupoes && orcamento.grupoes.length))
        }
        return this.orcamentoService
          .buscarOrcamento(route.parent.params.idOrcamento)
          .pipe(switchMap(res => of(!!(res.grupoes && res.grupoes.length))))
      })
    )
  }
}
