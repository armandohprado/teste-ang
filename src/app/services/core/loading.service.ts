import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  static noLoading: boolean
  static loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
  constructor() {}
}
