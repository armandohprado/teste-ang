import { Injectable } from '@angular/core'
import { FormGroup } from '@angular/forms'
import * as moment from 'moment'

@Injectable({
  providedIn: 'root',
})
export class ValidationService {
  constructor() {}

  static dateValidator(control: FormGroup) {
    if (!control.value) {
      return null
    }
    if (moment(control.value).isValid()) {
      return null
    }
    return { invalidDate: true }
  }
}
