import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class CotacaoService {
  gruposSelecionados = [
    { id: 123, name: 'Portas e Batentes de Madeira' },
    { id: 124, name: 'Portas e Batentes de Madeira 2' },
    { id: 125, name: 'Portas e Batentes de Madeira 3' },
    { id: 126, name: 'Portas e Batentes de Madeira 4' },
    { id: 127, name: 'Portas e Batentes de Madeira 5' },
    { id: 128, name: 'Portas e Batentes de Madeira 6' },
    { id: 129, name: 'Potas e Batentes de Madeira' },
    { id: 130, name: 'Portras e Batentes de Madeira' },
    { id: 131, name: 'Potars e Batentes de Madeira' },
    { id: 132, name: 'Protasr e Batentes de Madeira' },
    { id: 133, name: 'Partas e Batentes de Madeira' },
  ]

  familia = [
    {
      idFamilia: 100,
      numeroFamilia: 1,
      ordemFamilia: 1,
      descricaoFamilia: 'Arquitetura',
      grupoes: [
        {
          idGrupao: 100.01,
          numeroGrupao: 102,
          descricaoGrupao: 'Divisórias',
          grupos: [
            {
              idGrupo: 100.001,
              codigoGrupo: 100.001,
              nomeGrupo: 'Divisoria de Gessos',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 200.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              idGrupo: 109.002,
              codigoGrupo: 109.002,
              nomeGrupo: 'Divisoria de Gessos 2',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 190.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              idGrupo: 109.003,
              codigoGrupo: 109.003,
              nomeGrupo: 'Divisoria de Gessos 3',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 255.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              idGrupo: 109.004,
              codigoGrupo: 109.004,
              nomeGrupo: 'Divisoria de Gessos 4',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 144.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
          ],
        },
        {
          idGrupao: 100.002,
          numeroGrupao: 101,
          descricaoGrupao: 'Portas',
          grupos: [
            {
              idGrupo: 109.003,
              codigoGrupo: 109.003,
              nomeGrupo: 'Porta e Batentes de Madeira',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 200.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              idGrupo: 109.004,
              codigoGrupo: 109.004,
              nomeGrupo: 'Porta e Batentes de Madeira 2',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 190.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 109.005,
              codigoGrupo: 109.005,
              nomeGrupo: 'Porta e Batentes de Madeira 3',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 255.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 109.006,
              codigoGrupo: 109.006,
              nomeGrupo: 'Porta e Batentes de Madeira 4',
              descricaoComplementarGrupo: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 144.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
          ],
        },
      ],
    },
    {
      idFamilia: 107,
      numeroFamilia: 2,
      ordemFamilia: 2,
      descricaoFamilia: 'Engenharia',
      grupoes: [
        {
          idGrupao: 107.01,
          numeroGrupao: 108,
          descricaoGrupao: 'Estrutura',
          grupos: [
            {
              id: 107.001,
              nomeGrupo: 'Vigas',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 200.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.002,
              nomeGrupo: 'Vigas 2',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 190.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.003,
              nomeGrupo: 'Vigas 3',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 255.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.004,
              nomeGrupo: 'Vigas 4',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 144.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
          ],
        },
        {
          idGrupao: 107.02,
          numeroGrupao: 107,
          descricaoGrupao: 'Parte Elétrica',
          grupos: [
            {
              id: 107.002,
              nomeGrupo: 'Circuitos Elétricos',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 200.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.004,
              nomeGrupo: 'Circuitos Elétricos 2',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 190.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.005,
              nomeGrupo: 'Circuitos Elétricos 3',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 255.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
            {
              id: 107.006,
              nomeGrupo: 'Circuitos Elétricos 4',
              descricao: 'Sala do Presidente',
              datas: {
                escopo: '06/06/2019 - 12:00',
                custos: '06/06/2019 - 12:00',
              },
              valores: { meta: 144.0 },
              responsaveis: [
                {
                  descricao: 'gerentes',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 1',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'arquitetura',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 5',
                      cargo: 'Gerente de alguma coisa',
                    },
                    {
                      id: 1,
                      nome: 'Gustavo 6',
                      cargo: 'Subgerente de alguma coisa',
                    },
                  ],
                },
                {
                  descricao: 'especialista',
                  lista: [
                    {
                      id: 1,
                      nome: 'Gustavo 7',
                      cargo: 'Gerente de alguma coisa',
                    },
                  ],
                },
              ],
              comentarios: { texto: '', status: false },
              orcamentos: [],
              fornecedor: [
                {
                  proposal: 214235,
                  version: 'v1',
                  nameFornecedor: 'A|W estimado',
                  favorite: false,
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: '',
                    third: 'blue',
                  },
                  budgetValue: '100.000,00',
                },
                {
                  proposal: 100006,
                  favorite: true,
                  version: 'v2',
                  nameFornecedor: 'A|W especialista',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit2: true,
                    time: true,
                  },
                  status: {
                    first: 'green',
                    second: 'red',
                    third: 'blue',
                  },
                  budgetValue: '200.000,00',
                },
                {
                  proposal: 23452,
                  favorite: false,
                  version: 'v3',
                  nameFornecedor: 'Insights Marcenaria Técnica',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: 'red',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '110.000,00',
                },
                {
                  proposal: 742345,
                  favorite: false,
                  version: 'v7',
                  nameFornecedor: 'Jóia Marcenaria',
                  iconsActive: {
                    info: true,
                    send: true,
                    resend: true,
                    edit: true,
                    time: true,
                  },
                  status: {
                    first: '',
                    second: 'blue',
                    third: 'green',
                  },
                  budgetValue: '150.000,00',
                },
              ],
            },
          ],
        },
      ],
    },
  ]

  orcamentoFornecedorInfo = [
    {
      descricao: {
        grupoID: 107.003,
        valores: {
          total: 200.0,
          servico: 27.0,
          produto: 173.0,
        },
        entregas: {
          entrega: 15,
          execcao: 90,
        },
        datas: {
          solicitacao: '16/09/2010 as 10:00',
          retorno: '29/09/2010 as 10:00',
        },
      },
      info: {
        fantasia: 'Gustavo Empreendimentos',
        razao: 'Construtora e Associados',
        endereco: 'Rua Augusta, numero qualquer',
        cnpj: '00001.011/0001-500',
        ie: '065.151.156/15',
        im: '980.185',
      },
      contatos: [
        {
          nome: 'Gustavo 1',
          email: 'gustavo.yukio@soaexpert.com.br 1',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
        {
          nome: 'Gustavo 2',
          email: 'gustavo.yukio@soaexpert.com.br 2',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
      ],
    },
    {
      descricao: {
        grupoID: 107.004,
        valores: {
          total: 200.0,
          servico: 27.0,
          produto: 173.0,
        },
        entregas: {
          entrega: 15,
          execcao: 90,
        },
        datas: {
          solicitacao: '16/09/2010 as 10:00',
          retorno: '29/09/2010 as 10:00',
        },
      },
      info: {
        fantasia: 'Gustavo Empreendimentos',
        razao: 'Construtora e Associados',
        endereco: 'Rua Augusta, numero qualquer',
        cnpj: '00001.011/0001-500',
        ie: '065.151.156/15',
        im: '980.185',
      },
      contatos: [
        {
          nome: 'Gustavo 3',
          email: 'gustavo.yukio@soaexpert.com.br 1',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
        {
          nome: 'Gustavo 4',
          email: 'gustavo.yukio@soaexpert.com.br 2',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
      ],
    },
    {
      descricao: {
        grupoID: 100.001,
        valores: {
          total: 200.0,
          servico: 27.0,
          produto: 173.0,
        },
        entregas: {
          entrega: 15,
          execcao: 90,
        },
        datas: {
          solicitacao: '16/09/2010 as 10:00',
          retorno: '29/09/2010 as 10:00',
        },
      },
      info: {
        fantasia: 'Gustavo Empreendimentos',
        razao: 'Construtora e Associados',
        endereco: 'Rua Augusta, numero qualquer',
        cnpj: '00001.011/0001-500',
        ie: '065.151.156/15',
        im: '980.185',
      },
      contatos: [
        {
          nome: 'Gustavo 5',
          email: 'gustavo.yukio@soaexpert.com.br 1',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
        {
          nome: 'Gustavo 6',
          email: 'gustavo.yukio@soaexpert.com.br 2',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
      ],
    },
    {
      descricao: {
        grupoID: 100.002,
        valores: {
          total: 200.0,
          servico: 27.0,
          produto: 173.0,
        },
        entregas: {
          entrega: 15,
          execcao: 90,
        },
        datas: {
          solicitacao: '16/09/2010 as 10:00',
          retorno: '29/09/2010 as 10:00',
        },
      },
      info: {
        fantasia: 'Gustavo Empreendimentos',
        razao: 'Construtora e Associados',
        endereco: 'Rua Augusta, numero qualquer',
        cnpj: '00001.011/0001-500',
        ie: '065.151.156/15',
        im: '980.185',
      },
      contatos: [
        {
          nome: 'Gustavo 7',
          email: 'gustavo.yukio@soaexpert.com.br 1',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
        {
          nome: 'Gustavo 8',
          email: 'gustavo.yukio@soaexpert.com.br 2',
          tel: '(11)3721-8454',
          mobile: '(11)98415-5114',
        },
      ],
    },
    {
      descricao: {
        grupoID: 109.006,
        valores: {
          total: 200.0,
          servico: 27.0,
          produto: 173.0,
        },
        entregas: {
          entrega: 15,
          execcao: 90,
        },
        datas: {
          solicitacao: '16/09/2010 as 10:00',
          retorno: '29/09/2010 as 10:00',
        },
      },
      info: {
        fantasia: 'Gustavo Empreendimentos',
        razao: 'Construtora e Associados',
        endereco: 'Rua Augusta, numero qualquer',
        cnpj: '00001.011/0001-500',
        ie: '065.151.156/15',
        im: '980.185',
      },
      contatos: [
        {
          nome: 'Gustavo 9',
          email: 'gustavo.yukio@soaexpert.com.br 1',
          tel: '(11)3721-8454',
          celular: '(11)98415-5114',
        },
        {
          nome: 'Gustavo 10',
          email: 'gustavo.yukio@soaexpert.com.br 2',
          tel: '(11)3721-8454',
          celular: '(11)98415-5114',
        },
      ],
    },
  ]

  constructor() {}

  public getGruposSelecionados(param: string) {
    return this.gruposSelecionados.filter((a: any) => {
      return a.name.toLowerCase().includes(param.toLowerCase())
    })
  }

  public getAllGruposSelecionados() {
    return this.gruposSelecionados
  }

  public getGruposCompleto() {
    // return this.grupos
  }

  public getFamilias() {
    return this.familia
  }

  public getFornecedorInfo(id: number) {
    return this.orcamentoFornecedorInfo.filter(item => {
      if (item.descricao.grupoID === id) {
        return item
      }
    })
  }
}
