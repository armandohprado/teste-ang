import { TestBed } from '@angular/core/testing';

import { EnvioDeCotacaoService } from './envio-de-cotacao.service';

describe('EnvioDeCotacaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnvioDeCotacaoService = TestBed.get(EnvioDeCotacaoService);
    expect(service).toBeTruthy();
  });
});
