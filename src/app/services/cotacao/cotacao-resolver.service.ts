import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs'
import { CotacaoService } from './cotacao.service'

@Injectable({
  providedIn: 'root',
})
export class CotacaoResolverService implements Resolve<any> {
  constructor(private cotacaoService: CotacaoService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.cotacaoService.getControleDeCotacao(route.params.idOrcamento)
  }
}
