import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject } from 'rxjs'
import { shareReplay } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class CotacaoService {
  targets = {
    getEnvioCotacao: `${environment.delfino}envio-cotacao/orcamento-grupo/`,
    getControleCotacao: `${environment.ivana}controle-cotacao/orcamento/`,
    patchToggleFornecedor: `${environment.ivana}controle-cotacao/orcamento/`,
  }

  public groupId = new BehaviorSubject(null)
  public currentIdSource = new BehaviorSubject(null)
  currentId$ = this.currentIdSource.asObservable()
  public propostaIdSources = new BehaviorSubject(null)
  idProposta$ = this.propostaIdSources.asObservable()
  public GrupoSources = new BehaviorSubject(null)

  constructor(private http: HttpClient) {}

  getEnvioDeCotacaoInfo(idGrupoOrcamento) {
    const target = this.targets.getEnvioCotacao + idGrupoOrcamento
    return this.http.get(target)
  }

  getControleDeCotacao(idOrcamento) {
    return this.http
      .get(this.targets.getControleCotacao + idOrcamento)
      .pipe(shareReplay(1))
  }

  getControleDeCotacaoGrupoOrcamento(idOrcamento, idGrupoOrcamento) {
    return this.http
      .get(
        `${this.targets.getControleCotacao +
          idOrcamento}/orcamento-grupo/${idGrupoOrcamento}`
      )
      .pipe(shareReplay(1))
  }

  getControleCotacaoHistorico(
    idGrupo,
    idGrupoOrcamento,
    idProposta,
    category?
  ) {
    let target = `${this.targets.getControleCotacao +
      idGrupo}/orcamento-grupo/${idGrupoOrcamento}/proposta/${idProposta}`

    if (category === 'historico') {
      target += '/historico'
    } else {
      target += '/proposta-comercial'
    }
    return this.http.get(target)
  }

  getControleFornecedorInfo(idGrupo, idGrupoOrcamento, idProposta) {
    const target = `${this.targets.getControleCotacao +
      idGrupo}/orcamento-grupo/${idGrupoOrcamento}/proposta/${idProposta}`
    return this.http.get(target)
  }

  setCurrentId(id: number) {
    this.currentIdSource.next(id)
  }

  setPropostaListaIds(lista) {
    this.propostaIdSources.next(lista)
  }

  setCurrentGroupId(lista) {
    this.GrupoSources.next(lista)
  }

  setGroupId(id) {
    this.groupId.next(id)
  }

  toggleProposta(idOrcamento, idGrupoOrcamento, proposta, status) {
    const target = `${this.targets.getControleCotacao +
      idOrcamento}/orcamento-grupo/${idGrupoOrcamento}/proposta/${proposta}/proposta-desativada`
    const obj = {
      idProposta: proposta,
      desativa: status,
    }
    return this.http.put(target, obj)
  }
}
