import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { Observable } from 'rxjs'
import { Site } from '../../models'

const projeto = 123

export interface SitesResponse {
  idProjeto: number
  sites: Site[]
}

@Injectable({
  providedIn: 'root',
})
export class EnvioDeCotacaoService {
  targets = {
    EnvioCotacaoFornecedores: `${environment.delfino}envio-cotacao/orcamento-grupo-fornecedor/`,
    getEnvioCotacao: `${environment.delfino}envio-cotacao/orcamento-grupo/`,
    sendFile: `${environment.delfino}upload-arquivo/`,
  }

  constructor(private http: HttpClient) {}

  getFornecedores(idOrcamentoGrupo, idGrupo) {
    const target = `${this.targets.EnvioCotacaoFornecedores + idOrcamentoGrupo}`
    return this.http.get(target)
  }

  setCheckedFornecedor(id, idOrcamentoGrupo) {
    const payload = {
      idOrcamentoGrupoFornecedor: 0,
      idOrcamentoGrupo,
      idFornecedor: id,
      favorito: false,
      situacaoFornecedor: 0,
      fornecedorInterno: false,
      fornecedor: null,
    }
    return this.http.post(this.targets.EnvioCotacaoFornecedores, payload)
  }

  sendFile(id, files) {
    const formData = new FormData()
    for (const file of files) {
      formData.append('file', file, file.name)
    }

    const headers = new HttpHeaders().append(
      'Content-Disposition',
      'multipart/form-data'
    )
    return this.http.post(`${this.targets.sendFile}2`, formData, {
      headers,
    })
  }

  retrieveSites(idProjeto: number): Observable<SitesResponse> {
    return this.http.get<SitesResponse>(
      `${environment.delfino}envio-cotacao/projeto-edificio/${idProjeto}`
    )
  }
}
