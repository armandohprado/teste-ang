import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Funcionario, Responsavel } from '../../models'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'
import { OrcamentoService } from './orcamento.service'

@Injectable({
  providedIn: 'root',
})
export class ResponsavelService {
  constructor(
    private http: HttpClient,
    private orcamentoService: OrcamentoService
  ) {}

  getResponsaveis(
    idOrcamento: number,
    IdGrupo: number,
    tipoResponsavel?: number,
    term?: string
  ): Observable<Funcionario[]> {
    let params = new HttpParams()

    if (tipoResponsavel) {
      params = params.set('tipoResponsavelEnum', tipoResponsavel as any)
    }

    if (term) {
      params = params.set('busca', term)
    }

    return this.http.get<Funcionario[]>(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/Grupos/${IdGrupo}/Responsaveis`,
      { params }
    )
  }

  saveResponsavel(responsavelData: any) {
    const { idOrcamento } = this.orcamentoService.orcamento$.value
    return this.http.post<Responsavel>(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/OrcamentoGrupoResponsavel`,
      responsavelData
    )
  }

  deleteResponsavel(idOrcamentoGrupoResponsavel: number) {
    return this.http.delete<Responsavel>(
      `${environment.ApiUrl}orcamentos/OrcamentoGrupoResponsavel/${idOrcamentoGrupoResponsavel}`
    )
  }
}
