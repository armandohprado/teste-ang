import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router'
import { Orcamento } from '../../models'
import { Observable } from 'rxjs'
import { OrcamentoService } from './orcamento.service'

@Injectable()
export class OrcamentoResolverService implements Resolve<Orcamento> {
  constructor(private orcamentoService: OrcamentoService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Orcamento> {
    return this.orcamentoService.buscarOrcamento(route.params.idOrcamento)
  }
}
