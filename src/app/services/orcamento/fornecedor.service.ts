import { Fase } from './../../models/fase'
import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Fornecedor, SituacaoFornecedor } from '../../models'
import { environment } from '../../../environments/environment'
import { map } from 'rxjs/operators'
import { Arquivo } from 'src/app/models/arquivo'

@Injectable({
  providedIn: 'root',
})
export class FornecedorService {
  constructor(private httpClient: HttpClient) {}

  getTodosFornecedoresDoGrupo(
    idOrcamento: number,
    idGrupo,
    situacao: SituacaoFornecedor
  ): Observable<Fornecedor[]> {
    const params = new HttpParams().set(
      'situacaoFornecedorEnum',
      (situacao as unknown) as string
    )

    return this.httpClient.get<Fornecedor[]>(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/Grupos/${idGrupo}/Fornecedores`,
      { params }
    )
  }

  addFornecedoresGrupo(
    idOrcamento: number,
    idOrcamentoGrupo: number,
    fornecedores: Fornecedor[]
  ) {
    return this.httpClient.post(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/Grupos/${idOrcamentoGrupo}/Fornecedores`,
      fornecedores
    )
  }

  updateFornecedorGrupo(
    idOrcamento: number,
    idOrcamentoGrupo: number,
    fornecedor: Fornecedor
  ) {
    return this.httpClient.put(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/Grupos/${idOrcamentoGrupo}/Fornecedores`,
      fornecedor
    )
  }

  getFornecedor(idFornecedor?: number): Observable<Fornecedor> {
    const _UrlFornecedor: string = 'http://127.0.0.1:9000/fornecedor.json'
    return this.httpClient.get<Fornecedor>(_UrlFornecedor)
  }
  getFase(): Observable<{ fases: Fase[] }> {
    const _UrlFase: string = 'http://127.0.0.1:9000/fases.json'
    return this.httpClient.get<{ fases: Fase[] }>(_UrlFase)
  }
  getArquivos(): Observable<{ arquivos: Arquivo[] }> {
    const _UrlArquivos: string = 'http://127.0.0.1:9000/arquivos.json'
    return this.httpClient.get<{ arquivos: Arquivo[] }>(`${_UrlArquivos}`)
  }
}
