import { TestBed } from '@angular/core/testing'

import { OrcamentoResolverService } from './orcamento-resolver.service'

describe('OrcamentoResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}))

  it('should be created', () => {
    const service: OrcamentoResolverService = TestBed.get(
      OrcamentoResolverService
    )
    expect(service).toBeTruthy()
  })
})
