import { TestBed } from '@angular/core/testing'

import { ProjetoResolverService } from './projeto-resolver.service'

describe('ProjetoResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}))

  it('should be created', () => {
    const service: ProjetoResolverService = TestBed.get(ProjetoResolverService)
    expect(service).toBeTruthy()
  })
})
