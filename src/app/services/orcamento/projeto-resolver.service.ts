import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router'
import { Projeto } from '../../models'
import { OrcamentoService } from './orcamento.service'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class ProjetoResolverService implements Resolve<Projeto> {
  constructor(private orcamentoService: OrcamentoService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Projeto> {
    return this.orcamentoService.retrieveProject(route.params.idProjeto)
  }
}
