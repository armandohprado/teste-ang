import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { shareReplay } from 'rxjs/operators'
import { OrcamentoService } from './orcamento.service'
import { environment } from '../../../environments/environment'
import { CurvaAbcGrupo } from '../../models'

@Injectable({
  providedIn: 'root',
})
export class CurvaABCService {
  grupos$: Observable<CurvaAbcGrupo[]> = this.http
    .get<CurvaAbcGrupo[]>(
      `${environment.curvaABC}ListarCurvaAbc/${this.orcamentoService.orcamento$.value.idOrcamento}`
    )
    .pipe(shareReplay(1))

  constructor(
    private http: HttpClient,
    private orcamentoService: OrcamentoService
  ) {}
}
