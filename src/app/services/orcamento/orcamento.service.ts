import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { BehaviorSubject, Observable, of, Subject } from 'rxjs'
import { Edificio, Familia, Grupao, Orcamento, Projeto } from '../../models'
import { environment } from '../../../environments/environment'
import { WINDOW_TOKEN } from '../../shared/tokens/window'
import {
  distinctUntilChanged,
  filter,
  map,
  shareReplay,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators'
import { Cacheable, CacheBuster } from 'ngx-cacheable'
import { IObservableCacheConfig } from 'ngx-cacheable/common/IObservableCacheConfig'
import { find } from 'lodash'
import { FamiliaCustomizada } from '../../models/familiaCustomizada'
import { Datas } from '../../models/datas'

export const cacheBuster$ = new Subject<void>()

const CACHE_SETTINGS: IObservableCacheConfig = {
  maxAge: 30000 * 60,
  cacheBusterObserver: cacheBuster$,
}

interface GetFamilia {
  idFamilia: number
  customizada: boolean
}

@Injectable({
  providedIn: 'root',
})
export class OrcamentoService {
  orcamento$: BehaviorSubject<Orcamento> = new BehaviorSubject<Orcamento>(null)
  familias$: Observable<Familia[]> = this.orcamento$.pipe(
    filter(orcamento => !!orcamento),
    switchMap(orcamento =>
      this.buscarFamilias().pipe(
        distinctUntilChanged(),
        map(familias => this.setSelectedGroupsToFamilies(orcamento, familias))
      )
    )
  )
  selectProjetoAction$: BehaviorSubject<number> = new BehaviorSubject<number>(0)

  soma$: BehaviorSubject<number> = new BehaviorSubject<number>(null)

  projeto$: Observable<Projeto> = this.selectProjetoAction$.pipe(
    switchMap(idProjeto => {
      return this.retrieveProject(idProjeto)
    }),
    shareReplay(1)
  )

  private currentOpenedFamilyIDAction: BehaviorSubject<
    GetFamilia
  > = new BehaviorSubject<GetFamilia>({} as GetFamilia)

  familia$: Observable<Familia> = this.currentOpenedFamilyIDAction.pipe(
    withLatestFrom(this.orcamento$),
    switchMap(([{ idFamilia, customizada }, orcamento]) => {
      return this.buscarFamilia(idFamilia, customizada).pipe(
        map(familia => this.setSelectedGroupsToFamily(orcamento, familia)),
        shareReplay(1)
      )
    })
  )

  private grupoesMap = new Map<number, Grupao>()

  constructor(
    private http: HttpClient,
    @Inject(WINDOW_TOKEN) private window: Window
  ) {}

  @Cacheable(CACHE_SETTINGS)
  retrieveProject(projetoId: number): Observable<Projeto> {
    return this.http
      .get<Projeto>(`${environment.ApiUrl}projetos/${projetoId}`)
      .pipe(shareReplay(1))
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  saveOrcamento(orcamento: Orcamento): Observable<Orcamento> {
    if (orcamento.idOrcamento) {
      return this.updateOrcamento(orcamento)
    }

    return this.http.post<Orcamento>(
      `${environment.ApiUrl}orcamentos`,
      orcamento
    )
  }

  updatePlanoOrcamento() {
    const {
      valorCustoMeta,
      valorPlanejadoVenda,
      idOrcamento,
    } = this.orcamento$.value

    return this.http.put(
      `${environment.ApiUrl}orcamentos/${idOrcamento}/planoOrcamento`,
      { valorCustoMeta, valorPlanejadoVenda }
    )
  }

  @Cacheable(CACHE_SETTINGS)
  retrieveBuildings(projetoId?: number): Observable<Edificio[]> {
    return this.http.get<Edificio[]>(
      `${environment.ApiUrl}projetos/${projetoId}/edificios`
    )
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  deleteOrcamento(idProjeto: number, idConfiguracaoOrcamento: number) {
    return this.http.delete(
      `${environment.ApiUrl}orcamentos/${idConfiguracaoOrcamento}`
    )
  }

  @Cacheable(CACHE_SETTINGS)
  buscarFamilias(): Observable<Familia[]> {
    if (this.orcamento$.value && this.orcamento$.value.idOrcamento) {
      return this.http.get<Familia[]>(
        `${environment.ApiUrl}orcamentos/${this.orcamento$.value.idOrcamento}/familias`
      )
    }
    return of([])
  }

  @Cacheable(CACHE_SETTINGS)
  buscarFamilia(idFamilia: number, customizada): Observable<Familia> {
    let params = new HttpParams()
    if (customizada) {
      params = params.set('customizada', customizada)
    }
    return this.http.get<Familia>(
      `${environment.ApiUrl}orcamentos/${this.orcamento$.value.idOrcamento}/familias/${idFamilia}`,
      { params }
    )
  }

  @Cacheable(CACHE_SETTINGS)
  buscarOrcamento(idOrcamento: number): Observable<Orcamento> {
    return this.http
      .get<Orcamento>(`${environment.ApiUrl}orcamentos/${idOrcamento}`)
      .pipe(tap(orcamento => this.orcamento$.next(orcamento)))
  }

  // Seleciona os grupos a serem salvos e guarda em um mapa
  selectGroup(grupao: Grupao) {
    const { grupos: originalGrupos, idGrupao, ...rest } = grupao

    // extrai a propriedade selecionado antes de salvar no mapa de grupões
    const grupos = originalGrupos
      .filter(grupo => grupo.selecionado)
      .map(({ selecionado, ...otherProps }) => ({ ...otherProps }))
    if (grupos.length) {
      this.grupoesMap.set(idGrupao, { idGrupao, ...rest, grupos } as Grupao)
    } else if (!grupos.length && this.grupoesMap.has(idGrupao)) {
      this.grupoesMap.delete(idGrupao)
    }
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  saveGrupos(): Observable<Orcamento> {
    const { grupoes, ...other } = this.orcamento$.value

    const payload: Grupao[] = []

    for (const [, gr] of this.grupoesMap) {
      payload.push(gr)
    }

    this.grupoesMap = new Map<number, Grupao>()

    return this.http.put<Orcamento>(
      `${environment.ApiUrl}orcamentos/${other.idOrcamento}/grupoes`,
      payload
    )
  }

  getFamilia(idFamilia: number, customizada?: boolean) {
    this.currentOpenedFamilyIDAction.next({ idFamilia, customizada })
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  salvarFamilia(familiaCustomizada: FamiliaCustomizada) {
    return this.http[familiaCustomizada.idFamiliaCustomizada ? 'put' : 'post']<
      Observable<FamiliaCustomizada>
    >(`${environment.ApiUrl}familiasCustomizadas`, {
      ...familiaCustomizada,
      idOrcamento: this.orcamento$.value.idOrcamento,
    })
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  deleteFamilia(idFamiliaCustomizada: number) {
    return this.http.delete(
      `${environment.ApiUrl}familiasCustomizadas/${idFamiliaCustomizada}`
    )
  }

  @CacheBuster({
    cacheBusterNotifier: cacheBuster$,
  })
  refreshOrcamento() {
    return this.buscarOrcamento(this.orcamento$.value.idOrcamento)
  }

  saveDatas(datas: Datas) {
    return this.http.put(
      `${environment.ApiUrl}orcamentos/${this.orcamento$.value.idOrcamento}/datas`,
      datas
    )
  }

  ordenaFamilias(familias: Familia[]) {
    // Pega os ids das famílias a serem ordenadas, o serviço espera um array de ids ordenados
    const ordem = familias.reduce((accumulator, currentValue) => {
      accumulator.push(currentValue.idOrcamentoFamilia)
      return accumulator
    }, [])

    return this.http.put(
      `${environment.ApiUrl}orcamentos/${this.orcamento$.value.idOrcamento}/OrdenaFamilia`,
      ordem
    )
  }

  private updateOrcamento(orcamento: Orcamento): Observable<Orcamento> {
    return this.http.put<Orcamento>(
      `${environment.ApiUrl}orcamentos/configuracao/${orcamento.idOrcamento}`,
      orcamento
    )
  }

  private setSelectedGroupsToFamilies(orcamento, familias) {
    const { grupoes } = orcamento
    familias.forEach(familia => {
      familia.grupoes = []
      if (grupoes && grupoes.length) {
        grupoes.forEach((grupao: Grupao) => {
          if (
            (familia.idFamilia && grupao.idFamilia === familia.idFamilia) ||
            (grupao.idFamiliaCustomizada &&
              grupao.idFamiliaCustomizada === familia.idFamiliaCustomizada)
          ) {
            familia.grupoes.push(grupao)
            this.grupoesMap.set(grupao.idGrupao, grupao)
          }
        })
      }
    })
    return familias
  }

  private setSelectedGroupsToFamily(orcamento: Orcamento, familia: Familia) {
    this.editGrupoes(familia, orcamento)
    return familia
  }

  private editGrupoes(f: Familia, orcamento: Orcamento) {
    const { grupoes } = orcamento
    if (grupoes && grupoes.length) {
      for (const grupao of grupoes) {
        const selectedGroups: Grupao = f.grupoes.find(
          gr =>
            (f.idFamilia &&
              f.idFamilia === grupao.idFamilia &&
              gr.idGrupao === grupao.idGrupao) ||
            (f.idFamiliaCustomizada &&
              f.idFamiliaCustomizada === grupao.idFamiliaCustomizada &&
              gr.idGrupao === grupao.idGrupao)
        )
        if (selectedGroups) {
          for (const grupo of grupao.grupos) {
            find(selectedGroups.grupos, [
              'idGrupo',
              grupo.idGrupo,
            ]).selecionado = true
          }
        }
      }
    }
  }
}
