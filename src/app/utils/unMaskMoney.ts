export const unMaskMoney = (value: any) => {
  return parseInt((value || '').toString().replace(/\D/g, ''), 10) / 100
}
