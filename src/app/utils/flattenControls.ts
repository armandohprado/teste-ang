import { AbstractControl, FormArray, FormGroup } from '@angular/forms'

export function flattenControls(form: AbstractControl): AbstractControl[] {
  let extracted: AbstractControl[] = [form]
  if (form instanceof FormArray || form instanceof FormGroup) {
    const children = Object.values(form.controls).map(flattenControls)
    extracted = extracted.concat(...children)
  }
  return extracted
}
