import PerfectScrollbar from 'perfect-scrollbar'

export const setPerfectScroll = (document: Document): PerfectScrollbar => {
  const element = document.querySelector('.modal') as HTMLElement
  return new PerfectScrollbar(element, {
    suppressScrollX: true,
  })
}
