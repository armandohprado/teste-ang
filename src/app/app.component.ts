import { Component, ElementRef, Inject, OnDestroy, OnInit } from '@angular/core'
import PerfectScrollbar from 'perfect-scrollbar'
import { DOCUMENT } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { BreadCrumbService } from './services/core/bread-crumb.service'

@Component({
  selector: 'app-root',
  template: `
    <app-lottie-loader></app-lottie-loader>
    <header>
      <app-navbar></app-navbar>
    </header>

    <main class="container-fluid main">
      <button class="btn btn-icon btn-sm p-0 menu" type="button">
        <i class="icon icon-menu"></i>
      </button>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <ol class="breadcrumb mb-0 bg-transparent p-0">
              <li class="breadcrumb-item"><a href="#">orçamento</a></li>
              <li aria-current="page" class="breadcrumb-item active">
                configuração de orçamento
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content container">
        <router-outlet></router-outlet>
      </div>
    </main>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100vh;
      }

      .main {
        margin-top: 60px;
        padding-top: 15px;
      }

      .menu {
        position: absolute;
      }
    `,
  ],
})
export class AppComponent implements OnInit, OnDestroy {
  private ps: PerfectScrollbar

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private elementRef: ElementRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private breadCrumbService: BreadCrumbService
  ) {}

  ngOnInit(): void {
    this.init()
  }

  ngOnDestroy(): void {
    this.ps.destroy()
  }

  private init() {
    this.document.documentElement.className += ' perfect-scrollbar-on'
    this.document.documentElement.classList.remove('perfect-scrollbar-off')
    this.ps = new PerfectScrollbar(this.elementRef.nativeElement, {
      suppressScrollX: true,
    })
  }
}
