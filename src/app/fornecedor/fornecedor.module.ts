import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { FornecedorRoutingModule } from './fornecedor-routing.module'
import { SharedModule } from '../shared/shared.module';
import { DevolucaoPropostaExterno } from './externo/devolucao-proposta.component'

@NgModule({
  declarations: [DevolucaoPropostaExterno],
  imports: [CommonModule, FornecedorRoutingModule, SharedModule],
})
export class FornecedorModule {}
