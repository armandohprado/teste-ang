import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { DevolucaoPropostaExterno } from './externo/devolucao-proposta.component';

const routes: Routes = [
  {
    path: '',
    component: DevolucaoPropostaExterno,
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FornecedorRoutingModule {}
