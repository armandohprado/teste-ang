import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-externo',
  template: `<app-devolucao-proposta></app-devolucao-proposta>`,
})
export class DevolucaoPropostaExterno implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
