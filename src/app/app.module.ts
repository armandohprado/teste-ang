import { BrowserModule } from '@angular/platform-browser'
import { LOCALE_ID, NgModule } from '@angular/core'
import localePt from '@angular/common/locales/pt'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { SharedModule } from './shared/shared.module'
import { registerLocaleData } from '@angular/common'
import {
  BsDatepickerModule,
  BsLocaleService,
  defineLocale,
  ptBrLocale,
  TabsModule,
  TooltipModule,
  TypeaheadModule,
} from 'ngx-bootstrap'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { WINDOW_TOKEN } from './shared/tokens/window'
import { LottieAnimationViewModule } from 'ng-lottie'
import { LoadingInterceptorService } from './interceptors/loading-interceptor.service'
import { LoadingService } from './services/core/loading.service'

registerLocaleData(localePt, 'pt')

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    LottieAnimationViewModule.forRoot(),
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [
    {
      provide: WINDOW_TOKEN,
      useValue: window,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptorService,
      multi: true,
      deps: [LoadingService],
    },
    { provide: LOCALE_ID, useValue: 'pt-Br' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private localeService: BsLocaleService) {
    ptBrLocale.invalidDate = ''
    defineLocale('custom', ptBrLocale)
    localeService.use('custom')
  }
}
