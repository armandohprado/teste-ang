import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { environment } from '../environments/environment'

const routes: Routes = [
  {
    path: '',
    redirectTo: `projetos/${environment.idProjeto}/orcamentos`,
    pathMatch: 'full',
  },

  {
    path: 'projetos/:idProjeto/orcamentos',
    loadChildren: () =>
      import('./orcamento/orcamento.module').then(m => m.OrcamentoModule),
  },
  {
    path: 'fornecedor',
    loadChildren: () =>
      import('./fornecedor/fornecedor.module').then(m => m.FornecedorModule),
  },
  {
    path: '**',
    redirectTo: '',
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
