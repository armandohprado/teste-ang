import { Injectable } from '@angular/core'
import { tap } from 'rxjs/operators'
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http'
import { LoadingService } from '../services/core/loading.service'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class LoadingInterceptorService implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // set loading to true
    if (!LoadingService.noLoading) {
      LoadingService.loading.next(true)
    }
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {
          // if the event is for http response
          if (event instanceof HttpResponse && !LoadingService.noLoading) {
            // set loading to false
            LoadingService.loading.next(false)
          }
        },
        (err: any) => {
          // if any error (not for just HttpResponse) we stop set loading to false
          LoadingService.loading.next(false)
        }
      )
    )
  }
}
