export interface FamiliaCustomizada {
  idFamiliaCustomizada: number
  nomeFamiliaCustomizada: string
  idOrcamento: number
}
