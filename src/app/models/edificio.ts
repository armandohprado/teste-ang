import { Pavimento } from './pavimento'

export interface Edificio {
  idEdificio: number
  nomeEdificio: string
  registroAtivo: boolean
  dataCriacao: Date
  dataAlteracao: Date
  idProjeto: number
  nomeUsuario: string
  nomeFantasiaEdificio: string
  idCondominio: number
  pavimentos: Pavimento[]
}
