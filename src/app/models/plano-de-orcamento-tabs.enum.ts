export enum PlanoDeOrcamentoTabs {
  GRUPOS = 'grupos',
  RESPONSAVEIS = 'responsaveis',
  FORNECEDORES = 'fornecedores',
  ESTRATEGIAS = 'estrategias',
  VALORES = 'valores',
}
