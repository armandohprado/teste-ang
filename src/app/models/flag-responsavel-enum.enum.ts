export enum FlagResponsavelEnum {
  Go = 1,
  Ppo = 2,
  Vertical = 3,
  Gpp = 4,
  Arq = 5,
}
