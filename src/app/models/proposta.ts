import { StatusProposta } from './status-proposta.enum'

export interface Proposta {
  idProposta: number
  versaoProposta: number
  retornoProposta: boolean
  valorTotalServicoProposta: number
  valorTotalProdutoProposta: number
  valorParcialProposta?: number
  valorTotalProposta: number
  prazoEntrega?: Date | string
  prazoExecucao?: Date | string
  dataSolicitacaoProposta?: Date | string
  dataRetornoProposta?: Date | string
  desatualizadoProposta?: any
  idOrcamentoCenarioGrupo: number
  idOrcamentoGrupoFornecedor: number
  comentarioProposta?: any
  desativaProposta: boolean
  declinadaProposta: boolean
  status: StatusProposta
  idFornecedor: number
}
