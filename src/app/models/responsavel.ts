import { Cargo } from './cargo'

export interface Responsavel {
  idFuncionario: number
  nomeFuncionario: string
  nomeFantasiaFuncionario: string
  loginRede: string
  emailFuncionario: string
  urlFoto: string
  foto: string
  idAgrupadorCargo: number
  cargo: Cargo
  idEquipe: number
  ativo: boolean
  dataCadastro: Date
}
