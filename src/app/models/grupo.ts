import { Funcionario } from './funcionario'
import { Fornecedor } from './fornecedor'
import { Proposta } from './proposta'

export interface Grupo {
  fornecedores: Fornecedor[]
  responsaveis: Funcionario[]
  especialistas: Funcionario[]
  arquitetura: Funcionario[]
  dataLimiteDefinicao: Date
  dataLimiteRecebimento: Date | string
  idGrupo: number
  idGrupao: number
  codigoGrupo: string
  nomeGrupo: string
  descricaoComplementarGrupo: string
  apoioVertical: boolean
  apoioArquitetura: boolean
  idTipoGrupo: number
  idOrcamentoGrupo: number
  selecionado: boolean
  valorMetaGrupo: number
  escopoEntregue: boolean
  custoEntregue: boolean
  idFamilia: number
  descricaoFamilia: string
  valorSelecionado: number
  complementoGrupo: string
  comentarioGrupo: string
  exibeComentarioPropostaGrupo: string
  propostas: Proposta[]
}
