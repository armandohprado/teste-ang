import { Grupo } from './grupo';
import { SituacaoFornecedor } from './situacao-fornecedor.enum'
import { Funcionario } from './funcionario';

export interface Fornecedor {
  idFornecedor: number
  idCanalEntrada: number
  cnpj: string
  nomeFantasia: string
  razaoSocial: string
  dataAberturaEmpresa: Date | string
  site: string
  inscricaoEstadual: string
  inscricaoMunicipal: string
  CREA: string
  CNAE: string
  quantidadeCLT: number
  faturamento: number
  descricaoNegocio: string
  obrasClientes: string
  seguroRespCivilProfissional: boolean
  simplesNacional: boolean
  contaBancariaIdBanco: number
  contaBancariaAgencia: string
  contaBancariaConta: string
  idFuncionario: number
  idFuncionarioAnalista: number
  prioridade: number
  categoria: number
  percentualConclusao: number
  idStatus: number
  dataCriacao: Date | string
  dataAlteracao: Date | string
  favorito: boolean
  situacao: typeof SituacaoFornecedor
  funcionarios: Funcionario[],
}
