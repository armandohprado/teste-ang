import { Grupao } from './grupao'

export interface Familia {
  idFamilia?: number
  numeroFamilia: number
  idFamiliaCustomizada?: number
  idOrcamentoFamilia?: number
  descricaoFamilia: string
  ordemFamilia: number
  grupoes: Grupao[]
}
