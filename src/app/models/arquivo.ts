export interface Arquivo {
  id: number
  nomeArquivo: string
  data: string
  src: string
}
