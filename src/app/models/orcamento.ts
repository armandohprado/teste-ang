import { Fase } from './fase'
import { Grupao } from './grupao'
import { Datas } from './datas'
import { Edificio } from './edificio'

export interface Orcamento {
  idOrcamento: number
  nomeOrcamento: string
  idProjeto: number
  registroAtivo: boolean
  datas: Datas
  edificios: Edificio[]
  fases: Fase[]
  grupoes: Grupao[]
  idProjetoModeloEap: number
  valorPlanejadoVenda: number
  valorCustoMeta: number
}
