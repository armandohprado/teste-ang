import { Orcamento } from './orcamento'
import { Funcionario } from './funcionario'

export interface Projeto {
  idProjeto: number
  numeroProjeto: string
  nomeProjeto: string
  idEquipeProjeto: number
  idContaNegocio: number
  metragemAbsoluta: number
  idTipoProjeto: number
  idEscopo: number
  idRegiao: number
  idAreaAtuacao: number
  percentualMargemContribuicao: number
  orcamentos: Orcamento[]
  responsaveis: Funcionario[]
}
