import { Orcamento } from './orcamento';
import { Grupo } from './grupo';

export interface OrcamentoGrupo {
  orcamento: Orcamento
  grupo: Grupo
  idOrcamentoGrupo: number
  idOrcamento: number
  idGrupo: number
  dataLimiteCustosOrcamentoGrupo: Date
  dataLimiteEntregaMercadoria: Date
  dataInicioExecucaoServico: Date
  dataFimExecucaoServico: Date
  mensagemEnvioCotacao: string
}
