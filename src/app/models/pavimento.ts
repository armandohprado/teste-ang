export interface Pavimento {
  idPavimento: number
  nomePavimento: string
  siglaPavimento: string
  ordem: number
  idEdificio: number
}
