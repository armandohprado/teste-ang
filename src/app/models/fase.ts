export interface Fase {
  idFase: number
  nomeFase: string
  dataInicioFase: Date
  dataFimFase: Date
  dataCriacao: Date
  dataAlteracao: Date
  nomeUsuario: string
  idOrcamento: number
}
