import { Fornecedor } from './fornecedor';

export interface ModalEnvioCotacaoStep2 {
  fornecedor: Fornecedor,
  mensagem: string,
  favorito: boolean
}
