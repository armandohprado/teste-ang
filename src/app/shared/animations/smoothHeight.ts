import { animate, style, transition, trigger } from '@angular/animations'

export const SmoothHeight = trigger('growHeight', [
  transition('void <=> *', []),
  transition(
    '* <=> *',
    [style({ height: '{{startHeight}}px', opacity: 0 }), animate('.5s ease')],
    {
      params: { startHeight: 0 },
    }
  ),
])
