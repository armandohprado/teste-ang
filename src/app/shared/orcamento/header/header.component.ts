import { Component, Input, OnInit } from '@angular/core'
import { Orcamento, Projeto } from '../../../models'

@Component({
  selector: 'app-orcamento-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() projeto: Projeto
  @Input() showBar = false
  @Input() orcamento: Orcamento

  constructor() {}

  ngOnInit() {}
}
