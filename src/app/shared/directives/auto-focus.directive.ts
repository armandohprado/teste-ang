import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core'

@Directive({
  selector: '[appAutoFocus]',
  exportAs: 'appAutoFocus',
})
export class AutoFocusDirective implements OnChanges {
  @Input() appAutoFocus: boolean

  constructor(private el: ElementRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.appAutoFocus || typeof this.appAutoFocus === 'undefined') {
      this.el.nativeElement.focus()
    }
  }
}
