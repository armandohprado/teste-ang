import { Directive, ElementRef, HostListener } from '@angular/core'
import { NgControl } from '@angular/forms'
import { CurrencyPipe } from '@angular/common'
import { unMaskMoney } from '../../utils'

@Directive({
  selector: '[appCurrency]',
})
export class CurrencyDirective {
  private currency = new CurrencyPipe('pt-BR')
  constructor(private el: ElementRef, private ngControl: NgControl) {}

  @HostListener('input', ['$event'])
  onInputChange(event: Event | any) {
    this.convert(event)
  }

  private convert(event) {
    const { value } = event.target
    if (!!value) {
      const maskedValue = this.currency.transform(unMaskMoney(value), ' ')
      this.ngControl.control.setValue(maskedValue)
    } else {
      this.ngControl.control.setValue('')
    }
  }
}
