import { AutoSizeDirective } from './auto-size.directive'
import { ElementRef } from '@angular/core'

describe('AutoSizeDirective', () => {
  const el = new ElementRef('<textarea></textarea>')

  it('should create an instance', () => {
    const directive = new AutoSizeDirective(el)
    expect(directive).toBeTruthy()
  })
})
