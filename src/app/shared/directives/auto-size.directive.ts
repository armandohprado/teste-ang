import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Renderer2,
} from '@angular/core'

@Directive({
  selector: 'textarea[appAutoSize]',
})
export class AutoSizeDirective implements AfterViewInit {
  @HostBinding('class.overflow-hidden')
  className = true

  constructor(private elem: ElementRef, private renderer: Renderer2) {}

  public ngAfterViewInit() {
    this.resize()
  }

  @HostListener('input')
  private resize() {
    const textarea = this.elem.nativeElement as HTMLTextAreaElement
    if (this.renderer) {
      // Reset textarea height to auto that correctly calculate the new height
      this.renderer.setStyle(textarea, 'height', 'auto')
      // Set new height
      this.renderer.setStyle(textarea, 'height', `${textarea.scrollHeight}px`)
    }
  }
}
