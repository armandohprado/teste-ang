import { Directive, Input } from '@angular/core'

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[templateVar]',
  exportAs: 'templateVar',
})
export class TemplateVariableDirective {
  @Input() templateVar: any

  constructor() {}
}
