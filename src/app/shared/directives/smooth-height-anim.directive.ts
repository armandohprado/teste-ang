import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
} from '@angular/core'

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[smoothHeight]',
})
export class SmoothHeightAnimDirective implements OnChanges, AfterViewInit {
  @Input()
  smoothHeight
  trigger: boolean
  startHeight: number

  constructor(private element: ElementRef) {}

  @HostBinding('@growHeight')
  get grow() {
    return { value: this.trigger, params: { startHeight: this.startHeight } }
  }

  setStartHeight() {
    this.startHeight = this.element.nativeElement.clientHeight
  }

  ngOnChanges(changes) {
    this.setStartHeight()
    this.trigger = !this.trigger
  }

  ngAfterViewInit(): void {
    this.setStartHeight()
  }
}
