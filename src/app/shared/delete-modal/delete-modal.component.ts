import { Component, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent implements OnInit {
  title: string
  closeBtnName: string
  bodyContent: string
  confirmBtnName: string
  confirmAction: () => void

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {}
}
