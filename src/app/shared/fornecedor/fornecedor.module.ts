import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FornecedorComponent } from './fornecedor.component'

@NgModule({
  declarations: [FornecedorComponent],
  imports: [CommonModule],
  exports: [FornecedorComponent],
})
export class FornecedorModule {}
