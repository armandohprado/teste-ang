export * from './aplicar-descontos/aplicar-descontos.component';
export * from './condicoes-fornecimento/condicoes-fornecimento.component';
export * from './enviar-proposta/enviar-proposta.component';
export * from './normas-recomendacoes/normas-recomendacoes.component';
export * from './preencher-orcamento/preencher-orcamento.component';
