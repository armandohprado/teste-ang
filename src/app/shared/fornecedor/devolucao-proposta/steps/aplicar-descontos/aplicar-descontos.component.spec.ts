import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicarDescontosComponent } from './aplicar-descontos.component';

describe('AplicarDescontosComponent', () => {
  let component: AplicarDescontosComponent;
  let fixture: ComponentFixture<AplicarDescontosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicarDescontosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplicarDescontosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
