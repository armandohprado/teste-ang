import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CondicoesFornecimentoComponent } from './condicoes-fornecimento.component';

describe('CondicoesFornecimentoComponent', () => {
  let component: CondicoesFornecimentoComponent;
  let fixture: ComponentFixture<CondicoesFornecimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CondicoesFornecimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CondicoesFornecimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
