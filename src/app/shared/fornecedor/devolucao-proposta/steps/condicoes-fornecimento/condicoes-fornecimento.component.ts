import { Fase } from './../../../../../models/fase'
import { FornecedorService } from './../../../../../services/orcamento/fornecedor.service'
import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-condicoes-fornecimento',
  templateUrl: './condicoes-fornecimento.component.html',
  styleUrls: ['./condicoes-fornecimento.component.scss'],
})
export class CondicoesFornecimentoComponent implements OnInit {
  fases$: Observable<Fase[]>

  constructor(private fornecedor: FornecedorService) {}

  ngOnInit() {
    this.fases$ = this.fornecedor.getFase().pipe(map(x => x.fases))
  }
  //alerta
  alert: string = `Para prosseguir faça a leitura das CONDIÇÕES GERAIS DE FORNECIMENTO necessário para o preenchimento da proposta.`
  show: boolean = true

  onClosed(dismissedAlert: any): void {
    this.show = !this.show
  }
  declinarProposta() {}
  imprimirProposta() {}
  salvarProposta() {}
}
