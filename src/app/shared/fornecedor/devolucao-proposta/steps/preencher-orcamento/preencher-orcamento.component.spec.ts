import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreencherOrcamentoComponent } from './preencher-orcamento.component';

describe('PreencherOrcamentoComponent', () => {
  let component: PreencherOrcamentoComponent;
  let fixture: ComponentFixture<PreencherOrcamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreencherOrcamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreencherOrcamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
