import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormasRecomendacoesComponent } from './normas-recomendacoes.component';

describe('NormasRecomendacoesComponent', () => {
  let component: NormasRecomendacoesComponent;
  let fixture: ComponentFixture<NormasRecomendacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormasRecomendacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormasRecomendacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
