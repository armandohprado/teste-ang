import { Arquivo } from './../../../../../models/arquivo'
import { FornecedorService } from './../../../../../services/orcamento/fornecedor.service'
import { Component, OnInit, OnChanges, DoCheck } from '@angular/core'
import { Observable } from 'rxjs'
import { map, count } from 'rxjs/operators'

@Component({
  selector: 'app-normas-recomendacoes',
  templateUrl: './normas-recomendacoes.component.html',
  styleUrls: ['./normas-recomendacoes.component.scss'],
})
export class NormasRecomendacoesComponent implements OnInit, OnChanges {
  constructor(private fornecedorService: FornecedorService) {}
  arquivos$: Observable<Arquivo[]>

  ngOnInit() {
    this.arquivos$ = this.fornecedorService
      .getArquivos()
      .pipe(map(x => x.arquivos))
  }
  ngOnChanges() {
    console.log('chamada change')
  }
}
