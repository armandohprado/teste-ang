import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviarPropostaComponent } from './enviar-proposta.component';

describe('EnviarPropostaComponent', () => {
  let component: EnviarPropostaComponent;
  let fixture: ComponentFixture<EnviarPropostaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviarPropostaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviarPropostaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
