import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DevolucaoPropostaComponent } from './devolucao-proposta.component'

describe('DevolucaoPropostaComponent', () => {
  let component: DevolucaoPropostaComponent
  let fixture: ComponentFixture<DevolucaoPropostaComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DevolucaoPropostaComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DevolucaoPropostaComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
