import { FornecedorService } from './../../../services/orcamento/fornecedor.service'
import {
  Component,
  OnInit,
  ViewChild,
  Inject,
  OnDestroy,
  Input,
} from '@angular/core'
import { TabsetComponent } from 'ngx-bootstrap'
import { DOCUMENT } from '@angular/common'
import { Observable } from 'rxjs'
import { Fornecedor } from 'src/app/models/fornecedor'

@Component({
  selector: 'app-devolucao-proposta',
  templateUrl: './devolucao-proposta.component.html',
  styleUrls: ['./devolucao-proposta.component.scss'],
})
export class DevolucaoPropostaComponent implements OnInit, OnDestroy {
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent
  @Input() cabecalho$: Observable<Fornecedor>

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private fornecedor: FornecedorService
  ) {}

  ngOnInit() {
    this.cabecalho$ = this.fornecedor.getFornecedor()
  }
  ngOnDestroy(): void {}

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true
  }
}
