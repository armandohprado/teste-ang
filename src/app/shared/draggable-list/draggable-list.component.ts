import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core'
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop'
import { SafeHtml } from '@angular/platform-browser'

@Component({
  selector: 'app-draggable-list',
  templateUrl: './draggable-list.component.html',
  styleUrls: ['./draggable-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraggableListComponent implements OnInit, OnChanges {
  @Input()
  allListLabel = 'Disponíveis:'
  @Input()
  selectedListLabel = 'Selecionados:'

  @Input()
  fieldValue = 'id'
  @Input() customTemplate: TemplateRef<any>

  @Input()
  fieldLabel = 'nome'

  @Output()
  selectedData: EventEmitter<any[]> = new EventEmitter()

  @HostBinding('class.row')
  row = true

  @Input()
  emptyMessage: any | SafeHtml = `Não há edifícios cadastrados <br/>
          Cadastre na Organização de Projeto`

  constructor() {}

  // tslint:disable-next-line:variable-name
  private _selectedList: any[]

  get selectedList() {
    return this._selectedList
  }

  @Input('selectedList')
  set selectedList(list: any[]) {
    this._selectedList = list.slice(0)
  }

  // tslint:disable-next-line:variable-name
  private _list: any[]

  get list(): any[] {
    return this._list
  }

  @Input('list')
  set list(data: any[]) {
    // Distinct all values from selected values
    this._list = (data || []).filter(
      item =>
        !this.selectedList.find(
          selectedItem =>
            selectedItem[this.fieldValue] === item[this.fieldValue]
        )
    )
  }

  trackByFn = item => item[this.fieldLabel]

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {}

  drop(event: CdkDragDrop<any[], any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      )
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      )
    }

    this.selectedData.emit(this.selectedList)
  }
}
