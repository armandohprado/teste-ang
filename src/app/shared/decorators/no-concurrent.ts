import { Observable } from 'rxjs'
import { share } from 'rxjs/operators'

export function NoConcurrent(
  target,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  // save a reference to the original method this way we keep the values currently in the
  // descriptor and don't overwrite what another decorator might have done to the descriptor.
  if (descriptor === undefined) {
    descriptor = Object.getOwnPropertyDescriptor(target, propertyKey)
  }
  const originalMethod: any = descriptor.value

  // editing the descriptor/value parameter
  descriptor.value = function() {
    const prop: any = target[propertyKey]
    const args: any = arguments
    if (prop.$$noConcurrent) {
      return prop.$$noConcurrent
    }

    const obs: Observable<any> = originalMethod.apply(this, args).pipe(share())
    prop.$$noConcurrent = obs
    return obs
  }
}
