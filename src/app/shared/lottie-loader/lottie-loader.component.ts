import { Component } from '@angular/core'
import { LoadingService } from '../../services/core/loading.service'
import { BehaviorSubject } from 'rxjs'

@Component({
  selector: 'app-lottie-loader',
  templateUrl: './lottie-loader.component.html',
  styleUrls: ['./lottie-loader.component.scss'],
})
export class LottieLoaderComponent {
  lottieConfig = {
    path: 'assets/data/animation/loading_light.json',
    renderer: 'canvas',
    loop: true,
    autoplay: true,
  }
  loading: BehaviorSubject<boolean> = LoadingService.loading

  constructor() {}
}
