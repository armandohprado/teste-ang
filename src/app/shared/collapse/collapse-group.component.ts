import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core'
import { Collapse } from '../../models/collapse'
import { CollapseDirective } from 'ngx-bootstrap'
import { CollapseComponent } from './collapse.component'

@Component({
  selector: 'app-collapse-group',
  templateUrl: './collapse-group.component.html',
  styleUrls: ['./collapse-group.component.scss'],
  providers: [{ provide: Collapse, useExisting: CollapseGroupComponent }],
})
export class CollapseGroupComponent implements OnInit, Collapse, OnDestroy {
  @Input()
  isOpen: boolean

  @Output() isOpenChange = new EventEmitter<boolean>()
  @Output() expand = new EventEmitter<any>()

  @ViewChildren('collapse')
  collapseDirective: QueryList<CollapseDirective>

  protected collapse: CollapseComponent

  constructor(@Inject(CollapseComponent) collapse: CollapseComponent) {
    this.collapse = collapse
  }

  hide(): void {
    this.collapseDirective.toArray()[0].hide()
    this.isOpenChange.emit(false)
  }

  ngOnInit() {}

  onExpand() {
    this.collapse.closeOtherPanels(this)
    this.expand.emit()
  }

  ngOnDestroy(): void {}
}
