import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { BsModalService } from 'ngx-bootstrap/modal'
import { ModalEnvioDeCotacaoComponent } from '../../../orcamento/cotacao/home/modal-envio-de-cotacao/modal-envio-de-cotacao.component'
import { CotacaoService } from '../../../services/cotacao/cotacao.service'

@Component({
  selector: 'app-cotacao-fornecedores-header',
  templateUrl: './cotacao-fornecedores-header.component.html',
  styleUrls: ['./cotacao-fornecedores-header.component.scss'],
})
export class CotacaoFornecedoresHeaderComponent implements OnInit {
  @Input()
  info

  @Output()
  toggleFornecedores = new EventEmitter()

  toggleFornecedoresStatus = false

  bsModalRef: BsModalRef

  constructor(
    private modalService: BsModalService,
    private cotacaoService: CotacaoService
  ) {}

  ngOnInit() {}

  openModalAtributos() {}

  openModalEnvioDeCotacao(id, idGrupo) {
    this.cotacaoService.setCurrentId(id)
    this.cotacaoService.setCurrentGroupId(idGrupo)
    this.bsModalRef = this.modalService.show(ModalEnvioDeCotacaoComponent, {
      class: 'modal-lg modal-envio-cotacao',
      initialState: {
        idOrcamento: this.info.idOrcamento,
      },
      ignoreBackdropClick: true,
    })
  }

  clickToggleFornecedores() {
    this.toggleFornecedoresStatus = !this.toggleFornecedoresStatus
    this.toggleFornecedores.emit(this.toggleFornecedoresStatus)
  }
}
