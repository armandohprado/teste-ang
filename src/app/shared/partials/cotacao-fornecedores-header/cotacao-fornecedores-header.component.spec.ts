import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CotacaoFornecedoresHeaderComponent } from './cotacao-fornecedores-header.component'

describe('CotacaoFornecedoresHeaderComponent', () => {
  let component: CotacaoFornecedoresHeaderComponent
  let fixture: ComponentFixture<CotacaoFornecedoresHeaderComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CotacaoFornecedoresHeaderComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CotacaoFornecedoresHeaderComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
