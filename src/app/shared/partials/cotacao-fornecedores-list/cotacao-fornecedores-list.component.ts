import { Component, Input, OnInit, ViewChild } from '@angular/core'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';

import { CotacaoService } from 'src/app/services/cotacao/cotacao.service';

import { ModalEnvioDeCotacaoComponent } from '../../../orcamento/cotacao/home/modal-envio-de-cotacao/modal-envio-de-cotacao.component';
import { ModalArquivosPropostaComponent } from '../../../orcamento/cotacao/home/modal-arquivos-proposta/modal-arquivos-proposta.component';
import { ModalHistoricoPropostaComponent }
  from '../../../orcamento/cotacao/home/modal-historico-proposta/modal-historico-proposta.component';

import { take } from 'rxjs/operators';

@Component({
  selector: 'app-cotacao-fornecedores-list',
  templateUrl: './cotacao-fornecedores-list.component.html',
  styleUrls: ['./cotacao-fornecedores-list.component.scss'],
})
export class CotacaoFornecedoresListComponent implements OnInit {

  fornecedorInfo;
  loading = { state: true }

  @Input()
  info;

  @Input()
  idOrcamento;

  constructor(private modalService: BsModalService, private cotacaoService: CotacaoService) {}

  bsModalRef: BsModalRef;

  ngOnInit() {}

  getNameFavorite(name: string, favorite: boolean) {
    if (favorite) {
      return `<i class="icon-star-full"></i> ${name}`
    }
    return name
  }

  abrirModalEnvioDeCotacao (id) {
    this.cotacaoService.setCurrentId(id);
    this.bsModalRef = this.modalService.show(ModalEnvioDeCotacaoComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: false,
    })
  }

  openModalArquivosProposta(grupo, proposta) {
    this.cotacaoService.setPropostaListaIds({ idOrcamento: this.idOrcamento, idOrcamentoGrupo: grupo, idProposta: proposta});
    this.bsModalRef = this.modalService.show(ModalArquivosPropostaComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true
    })
  }

  openModalHistoricoProposta(grupo, proposta) {
    this.cotacaoService.setPropostaListaIds({ idOrcamento: this.idOrcamento, idOrcamentoGrupo: grupo, idProposta: proposta});
    this.bsModalRef = this.modalService.show(ModalHistoricoPropostaComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
    })
  }

  setPopoverInfo (grupo, proposta) {
    this.loading.state = true;
    this.cotacaoService.getControleFornecedorInfo(this.idOrcamento, grupo, proposta)
    .pipe(take(1))
    .subscribe( data => {
      this.fornecedorInfo = data;
      this.loading.state = false;
    })
  }

  hideProposta (grupo, proposta, status) {
    this.cotacaoService.toggleProposta(this.idOrcamento, grupo, proposta, status)
    .pipe(take(1))
    .subscribe( data => {
      console.log(data);
    })
  }

  showValorParcial (event) {
    event.target.classList.toggle('active');
  }
}
