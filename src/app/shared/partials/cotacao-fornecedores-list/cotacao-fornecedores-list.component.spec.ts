import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CotacaoFornecedoresListComponent } from './cotacao-fornecedores-list.component'

describe('CotacaoFornecedoresListComponent', () => {
  let component: CotacaoFornecedoresListComponent
  let fixture: ComponentFixture<CotacaoFornecedoresListComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CotacaoFornecedoresListComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CotacaoFornecedoresListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
