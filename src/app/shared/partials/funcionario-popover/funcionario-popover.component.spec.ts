import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionarioPopoverComponent } from './funcionario-popover.component';

describe('FuncionarioPopoverComponent', () => {
  let component: FuncionarioPopoverComponent;
  let fixture: ComponentFixture<FuncionarioPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionarioPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionarioPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
