import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core'
import { Funcionario } from '../../../models'
import { ResponsavelService } from '../../../services/orcamento/responsavel.service'
import { BsModalRef, BsModalService, PopoverDirective } from 'ngx-bootstrap'
import { DeleteModalComponent } from '../../delete-modal/delete-modal.component'
import { switchMap, take } from 'rxjs/operators'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-funcionario-popover',
  templateUrl: './funcionario-popover.component.html',
  styleUrls: ['./funcionario-popover.component.scss'],
})
export class FuncionarioPopoverComponent implements OnInit {
  @HostBinding('classList') classes = ['rounded-circle overflow-hidden']

  @ViewChild(PopoverDirective, { static: true }) popover: PopoverDirective

  @Input()
  funcionario: Funcionario

  @Input() canChange = true

  @Input()
  placement = 'right' as any

  @Input() showActionBtn = true

  @Output() openResponsavelModal: EventEmitter<any> = new EventEmitter<any>()

  private deleteModalRef: BsModalRef

  get fotoUrl(): string {
    return environment.fotoUrl
  }

  constructor(
    private responsavelService: ResponsavelService,
    private modalService: BsModalService,
    private orcamentoService: OrcamentoService
  ) {}

  ngOnInit() {}

  delete() {
    this.deleteModalRef = this.modalService.show(DeleteModalComponent, {
      initialState: {
        title: 'excluir assistente',
        bodyContent: 'Tem certeza que deseja excluir?',
        confirmBtnName: 'excluir',
        closeBtnName: 'não excluir',
        confirmAction: () => this.deleteAction(),
      },
      class: 'modal-dialog-centered modal-sm delete-modal',
    })
  }

  private deleteAction() {
    this.responsavelService
      .deleteResponsavel(this.funcionario.idOrcamentoGrupoResponsavel)
      .pipe(
        switchMap(() => this.orcamentoService.refreshOrcamento()),
        take(1)
      )
      .subscribe(() => this.deleteModalRef.hide())
  }
}
