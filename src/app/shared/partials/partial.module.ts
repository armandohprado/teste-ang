import { PipesModule } from './../pipes/pipes.module';
import { CotacaoFornecedoresListComponent } from './cotacao-fornecedores-list/cotacao-fornecedores-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { CotacaoFornecedoresHeaderComponent } from './cotacao-fornecedores-header/cotacao-fornecedores-header.component';
import { FuncionarioPopoverComponent } from './funcionario-popover/funcionario-popover.component';

@NgModule({
  declarations: [
    CotacaoFornecedoresHeaderComponent,
    CotacaoFornecedoresListComponent,
    FuncionarioPopoverComponent
  ],
  imports: [
    CommonModule,
    PopoverModule.forRoot(),
    PipesModule
  ],
  exports: [
    CotacaoFornecedoresHeaderComponent,
    CotacaoFornecedoresListComponent,
    FuncionarioPopoverComponent
  ]
})

export class PartialModule { }
