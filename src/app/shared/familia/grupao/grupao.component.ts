import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core'
import { Grupao, Grupo } from '../../../models'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { cloneDeep } from 'lodash'

@Component({
  selector: 'app-grupao',
  templateUrl: './grupao.component.html',
  styleUrls: ['./grupao.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrupaoComponent implements OnInit {
  @Input()
  additionalClass: string
  @Input()
  selectionMode = false
  isCollapsed = true
  private grupaoCopy: Grupao

  constructor(private orcamentoService: OrcamentoService) {}

  get grupao() {
    return this.grupaoCopy
  }

  @Input()
  set grupao(grupao: Grupao) {
    this.grupaoCopy = cloneDeep<Grupao>(grupao)
  }

  get grupos() {
    if (this.grupao) {
      return this.grupao.grupos
    }
    return []
  }

  trackByFn: any = (grupao: Grupo) => grupao.idGrupo

  ngOnInit() {}

  onSelection() {
    this.orcamentoService.selectGroup(this.grupaoCopy)
  }

  addCustomClass() {
    if (this.additionalClass === 'cotacao') {
      return 'w-100 flex flex-column cotacao'
    }
    return null
  }
}
