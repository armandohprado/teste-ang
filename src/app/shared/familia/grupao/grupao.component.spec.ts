import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GrupaoComponent } from './grupao.component'

describe('GrupaoComponent', () => {
  let component: GrupaoComponent
  let fixture: ComponentFixture<GrupaoComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GrupaoComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupaoComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
