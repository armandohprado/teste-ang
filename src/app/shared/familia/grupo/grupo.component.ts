import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Grupo } from '../../../models'

@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html',
  styleUrls: ['./grupo.component.scss'],
})
export class GrupoComponent implements OnInit {
  @Input()
  selectionMode = false

  @Input()
  grupo: Grupo

  @Output()
  selection: EventEmitter<Grupo> = new EventEmitter()

  @Input()
  id: number

  @Input()
  atributos = false

  @Input()
  additionalClass

  constructor() {}

  ngOnInit() {}

  select() {
    this.selection.emit(this.grupo)
  }
}
