import { Component, Input, OnInit } from '@angular/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { ModalSelecionarGruposComponent } from './modal-selecionar-grupos/modal-selecionar-grupos.component'
import { Familia, Grupao } from '../../models'
import { OrcamentoService } from '../../services/orcamento/orcamento.service'
import { ModalNovaFamiliaComponent } from '../../orcamento/plano-de-orcamento/tabs-steps/tab-grupos/modal-nova-familia/modal-nova-familia.component'
import { DeleteModalComponent } from '../delete-modal/delete-modal.component'
import { switchMap, take } from 'rxjs/operators'

@Component({
  selector: 'app-familia',
  templateUrl: './familia.component.html',
  styleUrls: ['./familia.component.scss'],
})
export class FamiliaComponent implements OnInit {
  isCollapsed = true
  @Input()
  familia: Familia
  @Input()
  hasButton = true
  @Input()
  hasSelectedGroupsInfo = true
  @Input()
  additionalClass: string

  private bsModalRef: BsModalRef

  constructor(
    private bsModalService: BsModalService,
    private orcamentoService: OrcamentoService
  ) {}

  trackByFn: any = (grupao: Grupao) => grupao.idGrupao

  ngOnInit() {}

  abrirModalSelecionarGrupos() {
    if (this.familia.idFamiliaCustomizada) {
      this.orcamentoService.getFamilia(this.familia.idFamiliaCustomizada, true)
    } else {
      this.orcamentoService.getFamilia(this.familia.idFamilia)
    }
    this.bsModalRef = this.bsModalService.show(ModalSelecionarGruposComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
    })
  }

  editarFamiliaCustomizada() {
    this.bsModalService.show(ModalNovaFamiliaComponent, {
      class: 'modal-sm modal-dialog-centered modal-nova-familia',
      ignoreBackdropClick: true,
      initialState: { familia: this.familia },
    })
  }

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed
  }

  apagarFamiliaCustomizada() {
    const ref = this.bsModalService.show(DeleteModalComponent, {
      initialState: {
        title: 'excluir família',
        bodyContent: 'Tem certeza que deseja excluir?',
        confirmBtnName: 'excluir',
        closeBtnName: 'não excluir',
        confirmAction: () =>
          this.orcamentoService
            .deleteFamilia(this.familia.idFamiliaCustomizada)
            .pipe(
              switchMap(() => this.orcamentoService.buscarFamilias()),
              switchMap(() => this.orcamentoService.refreshOrcamento()),
              take(1)
            )
            .subscribe(() => ref.hide()),
      },
      class: 'modal-dialog-centered modal-sm delete-modal',
    })
  }
}
