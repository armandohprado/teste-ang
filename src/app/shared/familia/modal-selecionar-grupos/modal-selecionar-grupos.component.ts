import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { Familia } from '../../../models'
import { OrcamentoService } from '../../../services/orcamento/orcamento.service'
import { Observable } from 'rxjs'
import { setPerfectScroll } from '../../../utils'
import { DOCUMENT } from '@angular/common'
import PerfectScrollbar from 'perfect-scrollbar'
import { switchMap, take } from 'rxjs/operators'

@Component({
  selector: 'app-modal-selecionar-grupos',
  templateUrl: './modal-selecionar-grupos.component.html',
  styleUrls: ['./modal-selecionar-grupos.component.scss'],
})
export class ModalSelecionarGruposComponent implements OnInit, OnDestroy {
  familia$: Observable<Familia> = this.orcamentoService.familia$
  private ps: PerfectScrollbar

  constructor(
    private bsModalRef: BsModalRef,
    private orcamentoService: OrcamentoService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit() {
    this.ps = setPerfectScroll(this.document)
  }

  ngOnDestroy(): void {
    if (this.ps) {
      this.ps.destroy()
    }
  }

  close() {
    this.bsModalRef.hide()
  }

  save() {
    this.orcamentoService
      .saveGrupos()
      .pipe(
        take(1),
        switchMap(() => this.orcamentoService.refreshOrcamento())
      )
      .subscribe(() => this.close())
  }
}
