import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ModalSelecionarGruposComponent } from './modal-selecionar-grupos.component'

describe('ModalSelecionarGruposComponent', () => {
  let component: ModalSelecionarGruposComponent
  let fixture: ComponentFixture<ModalSelecionarGruposComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSelecionarGruposComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSelecionarGruposComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
