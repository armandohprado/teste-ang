import { Component, Input, OnInit } from '@angular/core'
import { CotacaoService } from '../../../services/cotacao/cotacao.service'

@Component({
  selector: 'app-propostas',
  templateUrl: './propostas.component.html',
  styleUrls: ['./propostas.component.scss'],
})
export class PropostasComponent implements OnInit {
  @Input() orcamento

  @Input()
  grupo

  orcamentoGrupoId
  collapses: any = {
    isOpen_0: true,
  }
  groupInfo = {}
  toggleFornecedoresStatus: boolean

  constructor(private cotacaoService: CotacaoService) {}

  ngOnInit() {}

  getItemInfo(idOrcamento, idOrcamentoGrupo) {
    if (this.collapses['isOpen_items_' + idOrcamentoGrupo] === true) {
      const itemInfo = this.cotacaoService
        .getControleDeCotacaoGrupoOrcamento(idOrcamento, idOrcamentoGrupo)
        .subscribe(data => {
          this.groupInfo[idOrcamentoGrupo] = data
          itemInfo.unsubscribe()
        })
    }
  }

  toggleFornecedores(event: boolean) {
    this.toggleFornecedoresStatus = event
  }
}
