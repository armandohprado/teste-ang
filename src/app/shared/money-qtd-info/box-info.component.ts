import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core'
import { FadeOutAnimation } from '../animations/fadeOut'

@Component({
  selector: 'app-box-info',
  templateUrl: './box-info.component.html',
  styleUrls: ['./box-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [FadeOutAnimation],
})
export class BoxInfoComponent implements OnInit {
  @Input() label: string
  @Input() classList: string[] | string
  @Input() valor: string | number
  @Input() boxWidth = 330

  constructor() {}

  ngOnInit() {}
}
