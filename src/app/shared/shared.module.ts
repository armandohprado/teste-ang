import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NavbarComponent } from './navbar/navbar.component'
import {
  BsDatepickerModule,
  BsDropdownModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  PopoverModule,
  TabsModule,
  TooltipModule,
  AlertModule,
} from 'ngx-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SubmitButtonComponent } from './submit-button/submit-button.component'
import { DraggableListComponent } from './draggable-list/draggable-list.component'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { DateFormatDirective } from './directives/date-format.directive'
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar'
import { DeleteModalComponent } from './delete-modal/delete-modal.component'
import { FamiliaComponent } from './familia/familia.component'
import { GrupaoComponent } from './familia/grupao/grupao.component'
import { GrupoComponent } from './familia/grupo/grupo.component'
import { ModalSelecionarGruposComponent } from './familia/modal-selecionar-grupos/modal-selecionar-grupos.component'
import { CollapseComponent } from './collapse/collapse.component'
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component'
import { RouterModule } from '@angular/router'
import { LottieLoaderComponent } from './lottie-loader/lottie-loader.component'
import { LottieAnimationViewModule } from 'ng-lottie'
import { HeaderComponent } from './orcamento/header/header.component'
import { CollapseGroupComponent } from './collapse/collapse-group.component'
import { CurrencyDirective } from './directives/currency.directive'
import { BoxInfoComponent } from './money-qtd-info/box-info.component'
import { PartialModule } from './partials/partial.module'
import { FuncionarioPopoverComponent } from './partials/funcionario-popover/funcionario-popover.component'
import { PipesModule } from './pipes/pipes.module'
import { PropostasComponent } from './familia/propostas/propostas.component'
import { AutoSizeDirective } from './directives/auto-size.directive'
import { SmoothHeightAnimDirective } from './directives/smooth-height-anim.directive'
import { TemplateVariableDirective } from './directives/template-variable.directive'
import { AutoFocusDirective } from './directives/auto-focus.directive'
import { CdkTableModule } from '@angular/cdk/table'
import { NgSelectModule } from '@ng-select/ng-select';
import {
  DevolucaoPropostaComponent,
  AplicarDescontosComponent,
  CondicoesFornecimentoComponent,
  PreencherOrcamentoComponent,
  NormasRecomendacoesComponent,
  EnviarPropostaComponent
} from './fornecedor/devolucao-proposta';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  suppressScrollY: false,
  scrollYMarginOffset: 10,
}

@NgModule({
  declarations: [
    NavbarComponent,
    SubmitButtonComponent,
    DraggableListComponent,
    DateFormatDirective,
    DeleteModalComponent,
    FamiliaComponent,
    GrupaoComponent,
    GrupoComponent,
    ModalSelecionarGruposComponent,
    CollapseComponent,
    BreadCrumbComponent,
    LottieLoaderComponent,
    HeaderComponent,
    CollapseGroupComponent,
    CurrencyDirective,
    BoxInfoComponent,
    PropostasComponent,
    AutoSizeDirective,
    SmoothHeightAnimDirective,
    TemplateVariableDirective,
    AutoFocusDirective,
    DevolucaoPropostaComponent,
    CondicoesFornecimentoComponent,
    NormasRecomendacoesComponent,
    PreencherOrcamentoComponent,
    AplicarDescontosComponent,
    EnviarPropostaComponent,
  ],
  exports: [
    NavbarComponent,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    CollapseModule,
    BsDropdownModule,
    ModalModule,
    TooltipModule,
    SubmitButtonComponent,
    DraggableListComponent,
    DateFormatDirective,
    PerfectScrollbarModule,
    BsDatepickerModule,
    DeleteModalComponent,
    FamiliaComponent,
    GrupaoComponent,
    GrupoComponent,
    TabsModule,
    ModalSelecionarGruposComponent,
    DragDropModule,
    CollapseComponent,
    BreadCrumbComponent,
    LottieAnimationViewModule,
    LottieLoaderComponent,
    HeaderComponent,
    CollapseGroupComponent,
    CurrencyDirective,
    BoxInfoComponent,
    PartialModule,
    FuncionarioPopoverComponent,
    PipesModule,
    PopoverModule,
    PropostasComponent,
    AutoSizeDirective,
    SmoothHeightAnimDirective,
    TemplateVariableDirective,
    AutoFocusDirective,
    CdkTableModule,
    NgSelectModule,
    DevolucaoPropostaComponent,
  ],
  entryComponents: [DeleteModalComponent, ModalSelecionarGruposComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule,
    DragDropModule,
    PerfectScrollbarModule,
    TabsModule,
    LottieAnimationViewModule,
    ButtonsModule.forRoot(),
    PopoverModule.forRoot(),
    PartialModule,
    PipesModule,
    CdkTableModule,
    NgSelectModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
})
export class SharedModule {}
