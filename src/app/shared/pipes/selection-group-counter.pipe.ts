import { Pipe, PipeTransform } from '@angular/core'
import { Grupao } from '../../models'

@Pipe({
  name: 'selectionGroupCounter',
})
export class SelectionGroupCounterPipe implements PipeTransform {
  transform(list: Grupao[]): number {
    let ret = -1 //
    const gr = list || ([] as Grupao[])
    gr.forEach(({ grupos: { length } }) => {
      if (length) {
        if (ret >= 0) {
          ret += length
        } else {
          ret = length
        }
      }
    })
    return ret
  }
}
