import { Pipe, PipeTransform } from '@angular/core'
import { OnlyNumbersPipe } from './only-numbers.pipe'

@Pipe({
  name: 'formattedCNPJ',
})
export class FormattedCNPJPipe implements PipeTransform {
  onlyNumbers = new OnlyNumbersPipe()
  transform(CNPJ: string): any {
    if (CNPJ && CNPJ !== '') {
      try {
        CNPJ = CNPJ ? this.onlyNumbers.transform(CNPJ.toString()) : CNPJ
        CNPJ = `${CNPJ.substring(0, 2)}.${CNPJ.substring(
          2,
          5
        )}.${CNPJ.substring(5, 8)}/${CNPJ.substring(8, 12)}-${CNPJ.substring(
          12,
          14
        )}`
        return CNPJ
      } catch (e) {
        return ''
      }
    }
  }
}
