import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'onlyNumbers',
})
export class OnlyNumbersPipe implements PipeTransform {
  transform(value = '') {
    if (value) {
      return value.replace(/[^\d]+/g, '')
    }
    return value
  }
}
