import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'formattedCep',
})
export class FormattedCepPipe implements PipeTransform {
  private re = /^([\d]{2})\.?([\d]{3})-?([\d]{3})/

  transform(value = ''): string {
    return (value || '').replace(this.re, '$1$2-$3')
  }
}
