import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'normalizeString',
})
export class NormalizeStringPipe implements PipeTransform {
  transform(str: string) {
    return str
      .normalize('NFKD')
      .replace(
        /[\u0300-\u036F\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~]/g,
        ''
      )
  }
}
