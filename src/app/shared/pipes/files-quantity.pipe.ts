import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'filesQuantity',
})
export class FilesQuantityPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    let txt = ''
    if (value == 1) txt = ' arquivo anexado'
    else txt = ' arquivos anexados'
    return value + txt
  }
}
