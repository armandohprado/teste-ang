import { Pipe, PipeTransform } from '@angular/core'
import { numeroEspecial } from './helpers'
import { OnlyNumbersPipe } from './only-numbers.pipe'

@Pipe({
  name: 'formattedTel',
})
export class FormattedTelPipe implements PipeTransform {
  transform(value): string {
    if (value) {
      let tel
      const receivedValue = value
        ? new OnlyNumbersPipe().transform(value)
        : value
      if (numeroEspecial(receivedValue) && receivedValue.length === 8) {
        tel = `${receivedValue.substring(0, 4)}-${receivedValue.substring(
          4,
          receivedValue.length - 4
        )}`
      } else if (numeroEspecial(receivedValue) && receivedValue.length > 8) {
        tel = `${receivedValue.substring(0, 4)}-${receivedValue.substring(
          4,
          receivedValue.length - 4
        )}-${receivedValue.substring(
          receivedValue.length - 4,
          receivedValue.length
        )}`
      } else if (receivedValue.length === 11) {
        tel = `(${receivedValue.substring(0, 2)}) ${receivedValue.substring(
          2,
          7
        )}-${receivedValue.substring(7, receivedValue.length)}`
      } else if (receivedValue.length <= 4) {
        tel = receivedValue.substring(0, 4)
      } else {
        tel = `(${receivedValue.substring(0, 2)}) ${receivedValue.substring(
          2,
          6
        )}-${receivedValue.substring(6, receivedValue.length)}`
      }
      return tel
    }
  }
}
