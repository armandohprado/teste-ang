import { Pipe, PipeTransform } from '@angular/core'
import { NormalizeStringPipe } from './normalize-string.pipe'

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  private normalize = new NormalizeStringPipe()
  transform(value, keys: string, term: string) {
    if (!term) {
      return value
    }
    return (value || []).filter(item =>
      keys.split(',').some(key => {
        return (
          item.hasOwnProperty(key) &&
          item[key] &&
          new RegExp(this.normalize.transform(term), 'gi').test(
            this.normalize.transform(item[key])
          )
        )
      })
    )
  }
}
