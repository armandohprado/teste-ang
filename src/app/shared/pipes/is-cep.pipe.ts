import { Pipe, PipeTransform } from '@angular/core'
import { OnlyNumbersPipe } from './only-numbers.pipe'

@Pipe({
  name: 'isCEP',
})
export class IsCEPPipe implements PipeTransform {
  transform(value = ''): boolean {
    return /^[0-9]{8}$/.test(new OnlyNumbersPipe().transform(value))
  }
}
