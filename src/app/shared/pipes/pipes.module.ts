import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormattedCepPipe } from './formatted-cep.pipe'
import { FormattedTelPipe } from './formatted-tel.pipe'
import { HasSelectedGroupsPipe } from './has-selected-groups.pipe'
import { IsCEPPipe } from './is-cep.pipe'
import { IsTelPipe } from './is-tel.pipe'
import { SearchPipe } from './search.pipe'
import { SelectEmployeePipe } from './select-employee.pipe'
import { SelectionGroupCounterPipe } from './selection-group-counter.pipe'
import { OnlyNumbersPipe } from './only-numbers.pipe'
import { FormattedCNPJPipe } from './formatted-cnpj.pipe'
import { SituacaoPipe } from './situacao.pipe'
import { SupplierRulePipe } from './supplier-rule.pipe'
import { CustomPercentPipe } from './custom-percent.pipe'
import { NormalizeStringPipe } from './normalize-string.pipe'
import { CalcularParcialPipe } from './calcular-parcial.pipe'
import { FilesQuantityPipe } from './files-quantity.pipe'

const pipes = [
  FormattedCepPipe,
  FormattedTelPipe,
  HasSelectedGroupsPipe,
  IsCEPPipe,
  IsTelPipe,
  SearchPipe,
  SelectEmployeePipe,
  SelectionGroupCounterPipe,
  OnlyNumbersPipe,
  FormattedCNPJPipe,
  SituacaoPipe,
  CalcularParcialPipe,
  FilesQuantityPipe,
]

@NgModule({
  declarations: [
    ...pipes,
    SupplierRulePipe,
    CustomPercentPipe,
    NormalizeStringPipe,
  ],
  exports: [...pipes, SupplierRulePipe, CustomPercentPipe, NormalizeStringPipe],
  imports: [CommonModule],
})
export class PipesModule {}
