import { Pipe, PipeTransform } from '@angular/core'
import { PercentPipe } from '@angular/common'

@Pipe({
  name: 'customPercent',
})
export class CustomPercentPipe implements PipeTransform {
  percentPipe = new PercentPipe('pt_Br')

  transform(
    value: number,
    total: number = 100,
    digitsInfo?: string,
    locale?: string
  ): string {
    const totalValue = value / total
    return this.percentPipe.transform(totalValue, digitsInfo, locale)
  }
}
