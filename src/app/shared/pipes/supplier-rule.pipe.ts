import { Pipe, PipeTransform } from '@angular/core'
import { Grupo, SupplierRule } from '../../models'

@Pipe({
  name: 'supplierRule',
})
export class SupplierRulePipe implements PipeTransform {
  transform(grupo: Grupo): string {
    const { fornecedores } = grupo
    if (fornecedores && fornecedores.length) {
      if (fornecedores.length > 1) {
        return SupplierRule.MULTIPLOS
      } else if (
        fornecedores[0].nomeFantasia.includes(SupplierRule.ESPECIALISTA)
      ) {
        return SupplierRule.ESPECIALISTA
      }
      return fornecedores[0].nomeFantasia
    }
    return SupplierRule.META
  }
}
