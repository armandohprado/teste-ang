import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calcularParcial'
})
export class CalcularParcialPipe implements PipeTransform {

  transform(value: number, total: number): any {
    if ( value === undefined || !value) {
      return '';
    } else {

      if (total === value) {
        return 'total';
      } else {
        return 'parcial';
      }

    }
  }

}
