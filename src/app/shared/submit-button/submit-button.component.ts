import { Component, Input, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { markFormAs } from '../../utils'

@Component({
  selector: 'app-submit-button',
  template: `
    <button
      [tooltip]="tooltip"
      triggers="click"
      [type]="btnType"
      [placement]="tooltipPlacement"
      container="body"
      [ngClass]="btnClass"
      (click)="setFormTouched()"
    >
      <i class="icon" [className]="iconClass" *ngIf="iconClass"></i>
      {{ btnText }}
    </button>
  `,
  styles: [
    `
      :host {
        display: inline-block;
      }
    `,
  ],
})
export class SubmitButtonComponent implements OnInit {
  @Input()
  btnClass: any = 'btn btn-primary btn-md'
  @Input()
  form: FormGroup
  @Input()
  btnText: string
  @Input()
  btnType = 'submit'
  @Input()
  tooltipPlacement = 'top'
  @Input()
  iconClass: string
  @Input()
  tooltipText = `Verifique os campos marcados em vermelho no formulário.
  Clique em cada campo para ver os detalhes de preenchimento.`

  constructor() {}

  ngOnInit(): void {}

  setFormTouched() {
    if (this.form && this.form.invalid) {
      markFormAs(this.form)
    }
  }

  get tooltip() {
    if (this.form.invalid) {
      return this.tooltipText
    }
  }
}
