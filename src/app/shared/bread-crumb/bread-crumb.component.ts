import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { IBreadcrumb } from '../../models'
import {
  ActivatedRoute,
  NavigationEnd,
  PRIMARY_OUTLET,
  Router,
} from '@angular/router'
import { BreadCrumbService } from '../../services/core/bread-crumb.service'
import { filter, takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-bread-crumb',
  templateUrl: './bread-crumb.component.html',
  styleUrls: ['./bread-crumb.component.scss'],
})
export class BreadCrumbComponent implements OnInit, OnDestroy {
  // All the breadcrumbs
  public breadcrumbs: IBreadcrumb[]
  @Input()
  public addClass: string
  private ROUTE_DATA_BREADCRUMB = 'breadcrumb'
  private ROUTE_PARAM_BREADCRUMB = 'breadcrumb'
  private PREFIX_BREADCRUMB = 'prefixBreadcrumb'
  // The breadcrumbs of the current route
  private currentBreadcrumbs: IBreadcrumb[]

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>()
  private ROUTE_DATA_ACTUAL_ROUTE = 'actualPath'

  public constructor(
    private breadcrumbService: BreadCrumbService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    breadcrumbService
      .get()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((breadcrumbs: IBreadcrumb[]) => {
        this.breadcrumbs = breadcrumbs as IBreadcrumb[]
      })
  }

  public hasParams(breadcrumb: IBreadcrumb) {
    return Object.keys(breadcrumb.params).length
      ? [breadcrumb.url, breadcrumb.params]
      : [breadcrumb.url]
  }

  public ngOnInit() {
    if (this.router.navigated) {
      this.generateBreadcrumbTrail()
    }

    // subscribe to the NavigationEnd event
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroyed$)
      )
      .subscribe(() => {
        this.generateBreadcrumbTrail()
      })
  }

  private generateBreadcrumbTrail() {
    // reset currentBreadcrumbs
    this.currentBreadcrumbs = []

    // get the root of the current route
    let currentRoute: ActivatedRoute = this.activatedRoute.root

    // set the url to an empty string
    let url = ''

    // iterate from activated route to children
    while (currentRoute.children.length > 0) {
      const childrenRoutes: ActivatedRoute[] = currentRoute.children
      let breadCrumbLabel = ''

      // iterate over each children
      childrenRoutes.forEach(route => {
        // Set currentRoute to this route
        currentRoute = route
        // Verify this is the primary route
        if (route.outlet !== PRIMARY_OUTLET) {
          return
        }
        const hasData = route.routeConfig && route.routeConfig.data
        const hasDynamicBreadcrumb: boolean = route.snapshot.params.hasOwnProperty(
          this.ROUTE_PARAM_BREADCRUMB
        )

        if (hasData || hasDynamicBreadcrumb) {
          /*
          Verify the custom data property "breadcrumb"
          is specified on the route or in its parameters.
          Route parameters take precedence over route data
          attributes.
          */
          if (hasDynamicBreadcrumb) {
            breadCrumbLabel = route.snapshot.params[
              this.ROUTE_PARAM_BREADCRUMB
            ].replace(/_/g, ' ')
          } else if (
            route.snapshot.data.hasOwnProperty(this.ROUTE_DATA_BREADCRUMB)
          ) {
            breadCrumbLabel = route.snapshot.data[this.ROUTE_DATA_BREADCRUMB]
          }
          if (
            route.snapshot.data.hasOwnProperty(this.ROUTE_DATA_ACTUAL_ROUTE)
          ) {
            url = `../../${route.snapshot.data[this.ROUTE_DATA_ACTUAL_ROUTE]}`
          }
          // Get the route's URL segment
          const routeURL: string = route.snapshot.url
            .map(segment => segment.path)
            .join('/')
          url += `/${routeURL}`
          // Cannot have parameters on a root route
          if (routeURL.length === 0) {
            route.snapshot.params = {}
          }
          // Add breadcrumb
          const breadcrumb: IBreadcrumb = {
            label: breadCrumbLabel,
            params: route.snapshot.params,
            url,
          }
          // Add the breadcrumb as 'prefixed'. It will appear before all breadcrumbs
          if (route.snapshot.data.hasOwnProperty(this.PREFIX_BREADCRUMB)) {
            this.breadcrumbService.storePrefixed(breadcrumb)
          } else {
            this.currentBreadcrumbs.push(breadcrumb)
          }
        }
      })
      this.breadcrumbService.store(this.currentBreadcrumbs)
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
