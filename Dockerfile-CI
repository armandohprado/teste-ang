
FROM node:12-alpine as builder

ENV SONAR_SCANNER_VERSION="4.1.0.1829" \
    NPM_CONFIG_PREFIX="/home/node/.npm-global" \
    NODE_PATH="/home/node/.npm-global/lib/node_modules" \
    CHROME_BIN="/usr/bin/chromium-browser" \
    CHROME_PATH="/usr/lib/chromium/"

RUN apk add --update openjdk8-jre-base chromium

WORKDIR /ng-app
COPY . .
RUN chown node:node -R /ng-app

USER node

RUN yarn global add typescript tslint @angular/cli
RUN yarn install --ignore-optional
RUN yarn run --verbose test-headless
RUN yarn run ng build -- --prod --output-path=dist

RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}.zip -O sonarscanner.zip && \
    unzip sonarscanner.zip && \
    rm sonarscanner.zip

RUN sonar-scanner-${SONAR_SCANNER_VERSION}/bin/sonar-scanner -Dproject.settings=./sonar-scanner.properties


FROM nginx:1.14.1-alpine

COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
