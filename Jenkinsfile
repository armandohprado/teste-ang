
pipeline {
    options {
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
    }

    triggers {
        pollSCM('H/3 * * * *')
    }

    agent {
        label 'docker'
    }

    environment {
        TAG = env.GIT_COMMIT.substring(0, 7)
        COMPANY = 'athie'
        PROJECT = 'focus'
        ARTIFACT = 'front-orcamento'
        IMAGE_NAME = "${COMPANY}-${PROJECT}-${ARTIFACT}"

        GATEWAY_PATHS = '/'
    }

    stages {
        stage('Checkout') {
            steps {
                slackSend (message: "${printBuildId()} - Build iniciado (<${env.BUILD_URL}|Open>)")
                checkout scm
            }
            post {
                failure {
                    slackSend (color: 'danger', message: "${printBuildId()} - Falha ao realizar checkout no Git (<${env.BUILD_URL}|Open>)")
                }
            }
        }
        stage('Build & Test & Publish QA Reports') {
            environment {
                SONAR_CREDS = credentials('sonar')
            }
            steps {
                sh "docker build --target runtime --build-arg=SONAR_URL='${env.SONAR_URL}' --build-arg=SONAR_USERNAME='${SONAR_CREDS_USR}' --build-arg=SONAR_PASSWORD='${SONAR_CREDS_PSW}' --tag ${IMAGE_NAME}:${TAG} --file Dockerfile-CI ."
            }
            post {
                failure {
                    slackSend (color: 'danger', message: "${printBuildId()} - Falha durante o estágio de build e testes (<${env.BUILD_URL}|Open>)")
                }
            }
        }
        // stage('Integration Test') {
        //    steps {
        //        sh 'docker-compose -f docker-compose.integration.yml up'
        //        sh "docker-compose -f docker-compose.integration.yml up --force-recreate --abort-on-container-exit"
        //        sh "docker-compose -f docker-compose.integration.yml down -v"
        //    }
        // }
        stage('Push Image to Registry') {
            when {
                branch 'develop'
            }
            environment {
                REGISTRY_CREDS = credentials('registry')
            }
            steps {
                sh "echo ${REGISTRY_CREDS_PSW} | docker login --username ${REGISTRY_CREDS_USR} --password-stdin ${env.REGISTRY_URL}"
                sh "docker tag ${IMAGE_NAME}:${TAG} ${env.REGISTRY_URL}/${IMAGE_NAME}:${TAG}"
                sh "docker push ${env.REGISTRY_URL}/${IMAGE_NAME}:${TAG}"
            }
            post {
                failure {
                    slackSend (color: 'danger', message: "${printBuildId()} - Falha durante push da imagem para o Registry (<${env.BUILD_URL}|Open>)")
                }
            }
        }
        stage('Deploy to Rancher') {
            when {
                branch 'develop'
            }
            steps {
                rancher (
                    credentialId: 'rancher',
                    endpoint: env.RANCHER_URL,
                    environmentId: env.RANCHER_ENVIRONMENT_ID,
                    service: "${env.RANCHER_APP_STACK}/${ARTIFACT}",
                    image: "${env.REGISTRY_URL}/${IMAGE_NAME}:${TAG}",
                    confirm: true,
                    startFirst: false,
                    ports: "",
                    environments: "",
                    timeout: 60
                )
                slackSend (color: 'good', message: "${printBuildId()} - Serviço implantado em http://${ARTIFACT}.${env.RANCHER_APP_STACK}.${env.RANCHER_ENVIRONMENT_URL} (Acesso via Kong)")
            }
            post {
                failure {
                    slackSend (color: 'danger', message: "${printBuildId()} - Falha durante o deploy no Rancher (<${env.BUILD_URL}|Open>)")
                }
            }
        }
        stage('Update Kong Route') {
            when {
                branch 'develop'
            }
            steps {
                script {
                    def KONG_SERVICE_DATA = groovy.json.JsonOutput.toJson([
                        name: "${ARTIFACT}",
                        url: "http://${ARTIFACT}.${env.RANCHER_APP_STACK}"
                    ])
                    def KONG_ROUTES_DATA = groovy.json.JsonOutput.toJson([
                        strip_path: false,
                        paths: GATEWAY_PATHS.split(',')
                    ])

                    def response = httpRequest(
                        url: "${env.KONG_ADMIN_URL}/services/${ARTIFACT}",
                        validResponseCodes: '200,404'
                    )

                    if (404 == response.status) {
                        httpRequest(
                            httpMode: 'POST',
                            contentType: 'APPLICATION_JSON',
                            requestBody: KONG_SERVICE_DATA,
                            url: "${env.KONG_ADMIN_URL}/services/"
                        )
                        httpRequest(
                            httpMode: 'POST',
                            contentType: 'APPLICATION_JSON',
                            requestBody: KONG_ROUTES_DATA,
                            url: "${env.KONG_ADMIN_URL}/services/${ARTIFACT}/routes"
                        )
                    }
                    slackSend (color: 'good', message: "${printBuildId()} - Kong URL: ${GATEWAY_URL}[${GATEWAY_PATHS}]")
                }
            }
            post {
                failure {
                    slackSend (color: 'danger', message: "${printBuildId()} - Falha durante a atualização das rotas no Kong (<${env.BUILD_URL}|Open>)")
                }
            }
        }
    }

    post {
        regression {
            slackSend (color: 'danger', message: "${printBuildId()} - Erro no job (<${env.BUILD_URL}|Open>)")
        }
        fixed {
            slackSend (color: 'good', message: "${printBuildId()} - Build de volta ao normal (<${env.BUILD_URL}|Open>)")
        }
        success {
            script {
                if (env.BRANCH_NAME != 'develop') {
                    slackSend (color: 'good', message: "${printBuildId()} - Sucesso! (<${env.BUILD_URL}|Open>)")
                }
            }
        }
    }
}

def printBuildId(params = null) {
    java.net.URLDecoder.decode("${env.JOB_NAME}:B${env.BUILD_NUMBER}")
}
