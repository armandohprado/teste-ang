
FROM node:12-alpine as builder

WORKDIR /ng-app
COPY . .

RUN yarn global @angular/cli && \
    yarn && \
    yarn run ng build -- --prod --output-path=dist


FROM nginx:1.14.1-alpine

COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
